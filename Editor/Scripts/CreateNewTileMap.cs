#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;

namespace proj_pcg
{
    /// <summary>
    /// Creates several default combinations in the context menu of the hierarchy
    /// </summary>
    public class CreateNewTileMap : Editor
    {
        private static GameObject Create(Type tileManager = null, Type tileMap = null, 
            Type oracle = null, Type heightMap = null, Type initialState = null,
            Type tileSaver = null, Type tileStats = null,
            Type pathfinding = null)
        {
            GameObject tileSystem = new GameObject("Tile System");
            
            tileSystem.AddComponent(tileManager ?? typeof(TileManager)); // can be: TileManager, OptimalOrderTileManager
            tileSystem.AddComponent(tileMap ?? typeof(RectangleTileMap)); // can be rectangle or hexagon
            tileSystem.AddComponent(heightMap ?? typeof(BiomeTerrainMap)); // can be basic or TODO
            tileSystem.AddComponent(initialState ?? typeof(InitialState));
            tileSystem.AddComponent(oracle ?? typeof(StandardOracle)); // can be standard or basic
            tileSystem.AddComponent(tileSaver ?? typeof(TileSaver));
            tileSystem.AddComponent(tileStats ?? typeof(TileStats));
            tileSystem.AddComponent(pathfinding ?? typeof(AStarPathfinding)); // can be Astar or VisualPathfinding
        
            Selection.activeObject = tileSystem;
            
            return tileSystem;
        }

        [MenuItem("GameObject/Tile System/Standard Rectangular")]
        public static void StandardRectangular() => Create();
        
        [MenuItem("GameObject/Tile System/Basic Rectangular")]
        public static void BasicRectangular() => Create(
            oracle: typeof(BasicConnectivityOracle), 
            heightMap: typeof(BasicTerrainMap));

        [MenuItem("GameObject/Tile System/Optimal Load Order Standard Rectangular")]
        public static void OptimalLoadOrderStandardRectangular() => Create(
            tileManager: typeof(OptimalOrderTileManager));

        [MenuItem("GameObject/Tile System/Optimal Load Order Basic Rectangular")]
        public static void OptimalLoadOrderBasicRectangular() => Create(
            tileManager: typeof(OptimalOrderTileManager),
            oracle: typeof(BasicConnectivityOracle), 
            heightMap: typeof(BasicTerrainMap));
        

        [MenuItem("GameObject/Tile System/Standard Rectangular + VisualPathfinder")]
        public static void StandardRectangularWVP() => Create(
            pathfinding: typeof(VisualPathfinder));

        [MenuItem("GameObject/Tile System/Optimal Load Order Standard Rectangular + VisualPathfinder")]
        public static void OptimalLoadOrderStandardRectangularWVP() => Create(
            tileManager: typeof(OptimalOrderTileManager),
            pathfinding: typeof(VisualPathfinder));

        [MenuItem("GameObject/Tile System/Optimal Load Order Basic Rectangular + VisualPathfinder")]
        public static void OptimalLoadOrderBasicRectangularWVP() => Create(
            tileManager: typeof(OptimalOrderTileManager),
            oracle: typeof(BasicConnectivityOracle), 
            heightMap: typeof(BasicTerrainMap),
            pathfinding: typeof(VisualPathfinder));
        
        [MenuItem("GameObject/Tile System/Standard Hexagonal")]
        public static void StandardHexagonal() => Create(
            tileMap: typeof(HexagonalTileMap));
        
        [MenuItem("GameObject/Tile System/Basic Hexagonal")]
        public static void BasicHexagonal() => Create(
            oracle: typeof(BasicConnectivityOracle), 
            heightMap: typeof(BasicTerrainMap),
            tileMap: typeof(HexagonalTileMap));

        [MenuItem("GameObject/Tile System/Optimal Load Order Standard Hexagonal")]
        public static void OptimalLoadOrderStandardHexagonal() => Create(
            tileManager: typeof(OptimalOrderTileManager),
            tileMap: typeof(HexagonalTileMap));

        [MenuItem("GameObject/Tile System/Standard Hexagonal + VisualPathfinder")]
        public static void StandardHexagonalWVP() => Create(
            pathfinding: typeof(VisualPathfinder),
            tileMap: typeof(HexagonalTileMap));
    }
}


#endif