#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace proj_pcg
{
    /// <summary>
    /// Adds button to TileManager that initializes the TilePickerWindow.
    /// Registers clicks when TileManager is selected and sends data to TilePickerWindow.
    /// </summary>
    [CustomEditor(typeof(TileManager), true)]
    public class TileManagerEditor : Editor
    {
        private int controlID;
        
        private Tool swappedTool;
        
        private Coords startDragCoords;

        private void OnEnable()
        {
            controlID = GUIUtility.GetControlID(FocusType.Keyboard);
        }

        /// <summary>
        /// custom editor for TileManager. Just adds a cool button and services clicks.
        /// </summary>
        private void OnSceneGUI()
        {
            TileManager tileManager = target as TileManager;
            
            // mouse down click on tile manager thing
            if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
            {
                if (TilePickerWindow.instance == null) 
                    TilePickerWindow.Show(tileManager);
                
                startDragCoords = GetMouseTilePosition(tileManager);
                TilePickerWindow.UpdateCoords(startDragCoords); // TODO upgrade to HashSet
                
                Event.current.Use();
            }
        }


        protected virtual HashSet<Coords> SelectedCoords(Coords from, Coords to)
        {
            HashSet<Coords> selectedTiles = new();

            int minX = Mathf.Min(from.x, to.x);
            int maxX = Mathf.Max(from.x, to.x);
            int minZ = Mathf.Min(from.z, to.z);
            int maxZ = Mathf.Max(from.z, to.z);

            for (int i = minX; i <= maxX; i++)
            {
                for (int ii = minZ; ii <= maxZ; ii++)
                {
                    selectedTiles.Add(new Coords(i, ii));
                }
            }

            return selectedTiles;
        }

        /// <summary>
        /// Converts mouse screen position to the click's nearest Coords
        /// </summary>
        /// <exception cref="Exception">If the mouse is pointing away from the tile system</exception>
        protected virtual Coords GetMouseTilePosition(TileManager tileManager)
        {
            Vector3 mousePosition = Event.current.mousePosition;
            Vector2 positionOnScreen = HandleUtility.GUIPointToScreenPixelCoordinate(mousePosition);

            return tileManager.GetTileNearestToClick(positionOnScreen);
        }

        /// <summary>
        /// Renders the inspector GUI with an additional button
        /// </summary>
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            EditorGUILayout.Space();

            // tile picker window button (this also opens automatically if user clicks on tilemap)
            if (GUILayout.Button("Open Tile Picker")) 
                TilePickerWindow.Show(target as TileManager);
        }
    }
}
#endif