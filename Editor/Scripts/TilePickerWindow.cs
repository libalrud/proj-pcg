#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace proj_pcg
{
    
    /// <summary>
    /// The window to pick preferred tile states for the InitialState.
    /// </summary>
    public class TilePickerWindow : EditorWindow
    {
        public static TilePickerWindow instance;
        
        /// <summary>
        /// the current or last tile manager
        /// </summary>
        private TileManager tileManager;
        private InitialState initialState;
        
        private int selectedTileID;
        private Group selectedTileGroup;
        private int selectedTileElev;

        private static Coords _activeCoords = new Coords(0,0);

        private Coords activeCoords
        {
            get => _activeCoords;
            set
            {
                _activeCoords = value;
                initialState.SetSelectedCoordinates(activeCoords);
                OnCoordsChanged();
            }
        }

        public void OnCoordsChanged()
        {
            selectedTileID = initialState.GetTileID(activeCoords);
            selectedTileGroup = initialState.GetTileGroup(activeCoords);
        }
        
        /// <summary>
        /// Called by InitialStateEditor when the tile system object is selected
        /// Updates the referenced tile manager
        /// </summary>
        public static void Show(TileManager tileManager)
        {
            // updates information bound to the window and opens window
            instance = GetWindow<TilePickerWindow>();
            instance.tileManager = tileManager;
            instance.initialState = tileManager.GetComponent<InitialState>();

            instance.ShowUtility();
        }

        /// <summary>
        /// Updates the coordinates selected, e.g. the ones to be edited.
        /// </summary>
        public static void UpdateCoords(Coords activeCoords)
        {
            if (instance == null)
            {
                _activeCoords = activeCoords;
                return;
            }

            instance.activeCoords = activeCoords;
            instance.Repaint();
        }

        private void OnGUI()
        {
            if (instance == null)
                return;

            if (titleContent.text != GetName()) 
                titleContent = new GUIContent(GetName());

            UpdateGUI();
        }

        private Vector2 scrollPosition;
        
        protected void UpdateGUI()
        {
            ResetAllButton();
            
            ShowSelectedCoords();

            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);

            TileGroupOptions(tileManager.mainLayerGroups);
            TileGroupButton(initialState.HasTileGroup(activeCoords), selectedTileGroup == null);
            
            // TODO better (pre split group's tiles)
            IEnumerable<TileData> availableTiles = (initialState.HasTileGroup(activeCoords)
                ? initialState.GetTileGroup(activeCoords).compiledTiles
                : tileManager.mainLayerTileList);

            MainTileOptions(availableTiles);

            MainTileButton(initialState.HasTileID(activeCoords), 
                selectedTileID == -1);
            
            EditorGUILayout.EndScrollView();
        }

        private void TileGroupButton(bool isSet, bool shouldBeUnset)
        {
            EditorGUILayout.Space();
            
            if (!isSet && shouldBeUnset)
            {
                GUILayout.Label($"There is no tile group set.");
            }

            // is set and should be unset
            else if (isSet && shouldBeUnset)
            {
                initialState.UnsetTileGroup(activeCoords);
            }

            // is not set and should be set
            else if (!isSet && !shouldBeUnset)
            {
                initialState.SetTileGroup(selectedTileGroup); 
            }

            // is set and should be set
            else if (isSet && !shouldBeUnset)
            {
                if (initialState.GetTileGroup(activeCoords) == selectedTileGroup)
                {
                    GUILayout.Label($"Tile set to {selectedTileGroup}.");
                }

                else
                {
                    initialState.SetTileGroup(selectedTileGroup);
                }
            }
        }

        private void ShowSelectedCoords()
        {
            EditorGUILayout.LabelField($"Select preferred state for coordinates {activeCoords}");

            EditorGUI.BeginChangeCheck();
            Vector2Int newCoords = EditorGUILayout.Vector2IntField(
                "Current coordinates: ", activeCoords.ToVector2Int());

            if (EditorGUI.EndChangeCheck())
            {
                activeCoords = new Coords(newCoords);
            }
        }

        private void MainTileOptions(IEnumerable<TileData> availableTiles)
        {
            EditorGUILayout.Space();
            GUILayout.Label("Main Tile options:");

            foreach (var tile in availableTiles)
            {
                EditorGUI.BeginChangeCheck();
                bool selected = (tile.id == selectedTileID);

                GUILayout.Toggle(selected, tile.ToString());

                if (EditorGUI.EndChangeCheck())
                {
                    selectedTileID = (tile.id == selectedTileID) ? -1 : tile.id;
                }
            }
        }

        private void TileGroupOptions(Group[] groups)
        {
            EditorGUILayout.Space();
            GUILayout.Label("Tile Group options:");
            
            foreach (var group in groups)
            {
                EditorGUI.BeginChangeCheck();
                bool selected = group == selectedTileGroup;
                GUILayout.Toggle(selected, group.ToString());

                if (EditorGUI.EndChangeCheck())
                {
                    selectedTileGroup = (group == selectedTileGroup) ? null : group;
                }
            }
        }

        private void ResetAllButton()
        {
            if (GUILayout.Button("Reset all"))
            {
                initialState.ResetAll();
            }
            
            EditorGUILayout.Space();
        }

        /// <summary>
        /// is set = if there is a tile in the initial state
        /// should be unset = if the tile to be set is -1
        /// </summary>
        private void MainTileButton(bool isSet, bool shouldBeUnset)
        {
            EditorGUILayout.Space();
            
            if (!isSet && shouldBeUnset)
            {
                GUILayout.Label($"There is no tile set.");
            }

            // is set and should be unset
            else if (isSet && shouldBeUnset)
            {
                initialState.UnsetTileID(activeCoords);
            }

            // is not set and should be set
            else if (!isSet && !shouldBeUnset)
            {
                initialState.SetTileID(selectedTileID);
            }

            // is set and should be set
            else if (isSet && !shouldBeUnset)
            {
                if (initialState.GetTileID(activeCoords) == selectedTileID)
                {
                    GUILayout.Label($"Tile at {activeCoords} already set.");
                }
                else
                {
                    initialState.SetTileID(selectedTileID);
                }
            }
        }

        private string GetName() => $"Tile Picker ({tileManager.tileMapName})";
    }
}

#endif