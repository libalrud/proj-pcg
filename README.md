A tile-based procedural content generation tool. Part of my bachelor thesis.

## Installation with Unity Package Manager

In Unity, open the ”Package Manager” tab (Window -> Package Manager).
Select + -> Add package from Git URL.... Paste the Gitlab repository
URL (https://gitlab.fel.cvut.cz/libalrud/proj-pcg.git). Unity will do the rest
automatically.

## Getting started

The best way to get started is to take a look at the sample scenes in the Samples subdirectory.

One of Unity’s limitations is the inability to load read-only scenes. Package contents
are hard-coded to be read-only. To view, edit, or use a scene, copy it into your
project.

To create a new tile system, right-click in the hierarchy and select a premade option.
To create a new tile or tile border, right-click in the project window and select "TileData" or "BorderData"

Tooltips are available on all public fields. Code is documented and commented.
