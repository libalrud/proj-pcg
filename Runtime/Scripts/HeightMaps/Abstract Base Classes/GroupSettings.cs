using System;
using UnityEngine;

namespace proj_pcg
{
    /// <summary>
    /// Encapsulates all settings to be used by HeightMap.
    /// Should be expanded using inheritance.
    /// </summary>
    [Serializable]
    public abstract class GroupSettings : ScriptableObject
    {
        /// <summary>
        /// Which group the settings refers to
        /// </summary>
        [HideInInspector]
        public Group relevantGroup;

        /// <summary>
        /// The name of the group that is referred to.
        /// </summary>
        [SerializeField] [ReadOnly] [Tooltip("The name of the group that is referred to.")]
        public string groupName;

        /// <summary>
        /// The group ID of the group that is referred to
        /// </summary>
        [HideInInspector]
        public int groupID;
        
        /// <summary>
        /// The color of the gizmos for this group.
        /// </summary>
        [Tooltip("The color of the gizmos for this group.")] [SerializeField] 
        public Color gizmoColor;

        
        /// <summary>
        /// Instantiates and initializes a GroupSettings object.
        /// Should be called by 
        /// </summary>
        /// <param name="group">Which group is the Group settings relevant to</param>
        /// <typeparam name="T">A derived class from GroupSettings</typeparam>
        /// <returns>A new instance of the given scriptableObject class</returns>
        public static GroupSettings Init<T>(Group group) where T : GroupSettings
        {
            var newInstance = CreateInstance<T>();
            newInstance.InitGroupSettings(group);

            newInstance.name = $"{group}";
            
            return newInstance;
        }

        /// <summary>
        /// Called upon creation of a GroupSettings instance.
        /// </summary>
        private void InitGroupSettings(Group group)
        {
            relevantGroup = group;
            groupName = group.groupName;
            groupID = group.id;
                
            // https://gamedev.stackexchange.com/questions/46463/how-can-i-find-an-optimum-set-of-colors-for-10-players
            // https://web.archive.org/web/20130515024557/http://brpreiss.com/books/opus4/html/page214.html
            float hash = (group.id * 0.618034f) % 1.0f;
            gizmoColor = Color.HSVToRGB(hash, 0.8f, 0.5f + hash / 5f);
        }
    }
}