using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

namespace proj_pcg
{
    /// <summary>
    /// This class takes care of which group and which elevation should be where.
    /// </summary>
    [Serializable]
    public abstract class TerrainMap : TileSystemBase
    {
        /// <summary>
        /// Gizmos will visualize relevant information in the given range.
        /// </summary>
        [SerializeField] [Header("Editor settings")]
        [Tooltip("Gizmos will visualize relevant information in the given range.")]
        public int showGizmosInRange = 10;

        [FormerlySerializedAs("groupSettings")]
        public List<GroupSettings> allGroupSettings;

        /// <summary>
        /// Same as List groupSettings but searchable by group.
        /// </summary>
        protected Dictionary<Group, GroupSettings> groupSettingsByGroup;

        #region Internal methods

        public void Reset()
        {
            allGroupSettings ??= new List<GroupSettings>();
            
            OnValidate();
        }

        private void Start()
        {
            Reset();
        }

        public void OnValidate()
        {
            // update settings display
            UpdateInspector();
            
            // do all calculations
            Initialize(tileManager.mainLayerGroups.ToList());
        }

        #if UNITY_EDITOR
        
        /// <summary>
        /// Draws Gizmos with different colors for each group and different heights based on heights.
        /// </summary>
        private void OnDrawGizmos()
        {
            if(showGizmosInRange == 0)
                return;
            
            if(tileManager.mainLayerTileList.Count == 0)
                return;

            float approxTileSize = tileMap.GetApproximateTileRadius();
            Vector3 cameraPosition = UnityEditor.SceneView.currentDrawingSceneView.camera.transform.position;
            HashSet<Coords> nearCoords = tileManager.GetCoordsWithinRange(tileMap.GetNearestTile(cameraPosition), showGizmosInRange);

            foreach (Coords coords in nearCoords)
            {
                Group group = GetPreferredGroup(coords);

                int elevation = GetPrefferedHeight(coords);

                Vector3 position = tileMap.GetGlobalPositionOfCoords(coords) + Vector3.up * tileMap.elevationOffsetCoefficient * elevation;

                Gizmos.color = group == null ? Utils.transparent : groupSettingsByGroup[group].gizmoColor;
                
                Gizmos.DrawSphere(position, approxTileSize * 0.25f);
            }
        }
        #endif

        /// <summary>
        /// Manages the inspector list of group settings.
        /// If any group does not have a setting, it creates one and adds it into the list.
        /// </summary>
        protected void UpdateInspector()
        {
            // check render distance
            showGizmosInRange.Clamp(0, 100);

            // dont show both at once
            // if (initialState.showGizmosInRange > 0 && showGizmosInRange > 0)
            //     initialState.showGizmosInRange = 0;
            
            // if null, just to be sure
            allGroupSettings ??= new List<GroupSettings>();

            // remove duplicates and build up set of unique groups present
            HashSet<Group> currentGroups = new();

            for (int i = 0; i < allGroupSettings.Count; i++)
            {
                GroupSettings settings = allGroupSettings[i];

                if (settings == null || currentGroups.Contains(settings.relevantGroup))
                {
                    allGroupSettings.RemoveAt(i);
                }
                else
                {
                    currentGroups.Add(settings.relevantGroup);
                } 
            }

            // if invalid, reset
            if (!allGroupSettings.VerifyIDUniqueness(
                    s1 => s1.groupName, 
                    s2 => s2.groupID))
            {
                Debug.Log("Mismatched input group IDs in Heightmap. Reloading...");
                allGroupSettings.Clear();
            }

            foreach (Group group in tileManager.mainLayerGroups)
            {
                if (currentGroups.Contains(group))
                    continue;

                allGroupSettings.Add(CreateDefaultItem(group));
            }
            
            allGroupSettings.Sort((g1, g2) => (g1.relevantGroup.id - g2.relevantGroup.id));

            groupSettingsByGroup = allGroupSettings.ToDictionary(gs => gs.relevantGroup, gs => gs);
        }

        #endregion
        #region Utility methods

        /// <summary>
        /// Samples perlin noise
        /// </summary>
        /// <param name="coords">The coordinates to sample the Perlin noise from.</param>
        /// <param name="scale">The larger the scale, the larger the horizontal size of "hills"</param>
        /// <param name="rangeMin">The minimum output value</param>
        /// <param name="rangeMax">The maximum output value. The vertical hill size is equal to max - min.</param>
        /// <param name="offset">Horizontal offset to the coordinates. Perlin noise is symmetrical around 0, this can prevent it.</param>
        /// <returns>A value between rangeMin and rangeMax</returns>
        protected float GetPerlinNoise(Coords coords, 
            float scale = 1f, float rangeMin = 0.0f, float rangeMax = 1.0f, float offset = 0f)
        {
            float range = rangeMax - rangeMin;
            if (range == 0)
                return rangeMin;

            Vector2 perlinInputValue = coords * 0.291f / scale + new Vector2(offset, offset);
            float valueInRange = Mathf.PerlinNoise(perlinInputValue.x, perlinInputValue.y) * range; // from ~0 to ~range

            valueInRange = valueInRange.Clamp(0, range); // from zero to range
            valueInRange += rangeMin; // from min to max only
            return valueInRange;
        }

        #endregion
        #region Abstract methods

        /// <summary>
        /// Called in edit time upon OnValidate.
        /// Recalculate calculation coeffs and stuff here.
        /// </summary>
        /// <param name="groups">A list of relevant groups.</param>
        protected virtual void Initialize(List<Group> groups){}

        /// <summary>
        /// Returns the preferred tile group for the given coordinates.
        /// </summary>
        public abstract Group GetPreferredGroup(Coords coords);

        /// <summary>
        /// Returns the preferred tile height/elevation for the given coordinates.
        /// </summary>
        public abstract int GetPrefferedHeight(Coords coords);
        
        /// <summary>
        /// Do you want to select parameters for your tile group map generation manually?
        /// Override this method to use, for example, a derived class.
        /// </summary>
        /// <example>return GroupSettings.Init&lt;BiomeGroupSettings&gt;(relevantGroup);</example>
        protected abstract GroupSettings CreateDefaultItem(Group relevantGroup);
        
        #endregion
    }
}
