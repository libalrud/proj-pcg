using System;
using UnityEngine;

namespace proj_pcg
{
    /// <summary>
    /// Proof of concept inherited class from GroupSettings.
    /// Custom HeightMap settings can implement their own group settings.
    /// </summary>
    [Serializable]
    public class BasicGroupSettings : GroupSettings
    { }
}