using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace proj_pcg
{
    
    /// <summary>
    /// Primitive proof-of-concept height map.
    /// Example usage of given methods.
    /// </summary>
    public class BasicTerrainMap : TerrainMap
    {
        /// <summary>
        /// Proportional to how big a tile group typically is.
        /// </summary>
        [Tooltip("Proportional to how big a tile group typically is.")] 
        [SerializeField] [Header("Generic settings")]
        protected float groupWidthScale = 1f;

        /// <summary>
        /// Proportional to how high the terrain typically spans.
        /// </summary>
        [Tooltip("Proportional to how high the terrain typically spans.")] 
        [SerializeField]
        protected float groupHeightScale = 1f;

        /// <summary>
        /// If true, a random offset will be added to the Perlin noise map.
        /// If false, tile groups will have similar placements every time.
        /// </summary>
        [SerializeField] [Tooltip("If true, a random offset will be added to the Perlin noise map. " +
                                  "If false, tile groups will have similar placements every time.")]
        protected bool randomOffset = true;

        private float groupOffset;

        private void Start()
        {
            // only change offset upon Start
            groupOffset = randomOffset ? Random.Range(-10000, 10000) : 0;
        }

        /// <summary>
        /// Simple perlin-choice based group area selection
        /// </summary>
        /// <param name="coords"></param>
        /// <returns></returns>
        public override Group GetPreferredGroup(Coords coords)
        {
            try
            {
                return GetPerlinChoice(coords, tileManager.mainLayerGroups, scale: groupWidthScale, offset: groupOffset);
            }
            catch (ArithmeticException)
            {
                return null;
            }
        }

        /// <summary>
        /// Simple perlin noise height map
        /// </summary>
        public override int GetPrefferedHeight(Coords coords)
        {
            if (initialState.HasTileElev(coords))
            {
                return initialState.GetTileElev(coords);
            }

            if (groupHeightScale == 0)
            {
                return 0;
            }
            
            return (int)GetPerlinNoise(coords, groupHeightScale, 0, groupHeightScale, groupOffset);
        }

        /// <summary>
        /// Example usage of the CreateDefaultItem class.
        /// </summary>
        protected override GroupSettings CreateDefaultItem(Group relevantGroup)
        {
            GroupSettings res = GroupSettings.Init<BasicGroupSettings>(relevantGroup);

            res.name = $"{relevantGroup}";
            return res;

        }

        /// <summary>
        /// Simple perlin choice.
        /// </summary>
        /// <returns>A random choice based on Perlin noise</returns>
        /// <exception cref="ArithmeticException">If nothing to choose from.</exception>
        private T GetPerlinChoice<T>(Coords coords, IList<T> list, 
            float scale = 1f, float offset = 0f)
        {
            if (list.Count == 0)
                throw new ArithmeticException("Nothing to choose from!");

            if (scale == 0)
                return list[0];

            // designed to prevent a value of list.Count being returned
            // list.Count 1 -> 0.9999
            // this will break because of floating point accuracy
            float upperRangeCutoff = list.Count - 1e-5f;
            
            float choiceValue = GetPerlinNoise(coords, scale: scale, 
                rangeMin: 0, rangeMax: upperRangeCutoff, offset: offset);

            int choice = Mathf.FloorToInt(choiceValue % list.Count);

            if (choice <= -1 || choice >= list.Count)
            {
                Debug.LogError($"Choice: {choice} / {list.Count} ({choiceValue})");
            }
            
            return list[choice];
        }
    }
}
