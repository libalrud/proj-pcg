using System;
using UnityEngine;

namespace proj_pcg
{
    /// <summary>
    /// Proof of concept inherited class from GroupSettings.
    /// Custom HeightMap settings can implement their own group settings.
    /// </summary>
    [Serializable]
    public class BiomeGroupSettings : GroupSettings
    {
        private void OnValidate()
        {
            if (size < 0)
                size = 0;
            
            if (frequency < 0)
                frequency = 0;
            
            if (heightSpread < 0)
                heightSpread = 0;
            
            if (ruggedness < 0)
                ruggedness = 0;
        }

        /// <summary>
        /// Proportional to the size of the biome.
        /// </summary>
        [Tooltip("Proportional to the size of the biome.")] 
        public float size = 1;
        
        /// <summary>
        /// Proportional to the frequency of the biome
        /// </summary>
        [Tooltip("Proportional to the frequency of the biome")]
        public float frequency = 1;

        /// <summary>
        /// Middle elevation of the biome.
        /// </summary>
        [Tooltip("Middle elevation of the biome.")]
        public float middleElevation = 0;
        
        /// <summary>
        /// How much should the height vary.
        /// </summary>
        [Tooltip("How much should the height vary.")]
        public float heightSpread = 0;
        
        /// <summary>
        /// How frequently should the height vary.
        /// </summary>
        [Tooltip("How frequently should the height vary.")]
        public float ruggedness = 1;

        /// <summary>
        /// Random offset for Perlin noise to avoid symmetry.
        /// </summary>
        [ReadOnly] [Tooltip("Random offset for Perlin noise to avoid symmetry.")]
        public float randomOffset;
    }
}