using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace proj_pcg
{
    /// <summary>
    /// Full-fledged-ish biome map w/heights.
    /// </summary>
    public class BiomeTerrainMap : TerrainMap
    {
        /// <summary>
        /// Distance to blend height maps in. Calculation time scales quadratically. Recommended less than 5.
        /// </summary>
        [Range(0, 10)] [Header("Biome settings")] [Tooltip("Distance to blend height maps in. Calculation time scales " +
                                                           "quadratically. Recommended less than 5.")]
        public int heightBlendRange = 2;

        /// <summary>
        /// Cheaper blending, less accurate, but scales lineraly.
        /// </summary>
        [Tooltip("Cheaper blending, less accurate, but scales lineraly.")]
        public bool fastBlending = false;

        /// <summary>
        /// Don't want a flat biome to be blended towards others? Set this to true!
        /// </summary>
        [Tooltip("Don't want a flat biome to be blended towards others? Set this to true!")]
        public bool blendFlatBiomesLess = false;

        /// <summary>
        /// This will fix Gizmo maps not updating in Editor sometimes.
        /// </summary>
        [Tooltip("This will fix Gizmo maps not updating in Editor sometimes.")]
        public bool dontCacheInEditor = true;
        
        /// <summary>
        /// A cache for calculated values.
        /// </summary>
        private Dictionary<Coords, Group> preferredGroups;

        #region Override methods

        private void Awake()
        {
            preferredGroups = new Dictionary<Coords, Group>();
        }

        protected override void Initialize(List<Group> groups)
        {
            preferredGroups = new Dictionary<Coords, Group>();

            if (groupSettingsByGroup == null)
                OnValidate();
        }

        /// <summary>
        /// Samples all groups and picks the winner
        /// </summary>
        /// <param name="coords"></param>
        /// <returns></returns>
        public override Group GetPreferredGroup(Coords coords)
        {
            
            if (Application.isEditor && !Application.isPlaying && dontCacheInEditor) 
                preferredGroups = new Dictionary<Coords, Group>();
            else if (Application.isPlaying) 
                preferredGroups ??= new Dictionary<Coords, Group>();
            
            if (preferredGroups.ContainsKey(coords))
                return preferredGroups[coords];

            
            Dictionary<Group, float> candidates = allGroupSettings.ToDictionary(
                gs => gs.relevantGroup,
                gs => GetBiomeValue(coords, (BiomeGroupSettings) gs));

            Group result = candidates.MaxKey();

            preferredGroups[coords] = result;
            return result;
        }

        /// <summary>
        /// Calculates preferred height in a coordinate posision. Blends near biome profiles together.
        /// </summary>
        /// <param name="coords">The coordinates</param>
        /// <returns>An integer value of the preferred height.</returns>
        public override int GetPrefferedHeight(Coords coords)
        {
            BiomeGroupSettings currentBiomeSettings = groupSettingsByGroup[GetPreferredGroup(coords)] as BiomeGroupSettings;
            
            if (currentBiomeSettings!.heightSpread == 0)
                return currentBiomeSettings.middleElevation.Round();

            // sample elevation profile with several neighbors? or not???
            HashSet<Coords> nearCoords = GetSamples(coords);

            float totalWeight = 0;
            float totalHeight = 0;

            foreach (Coords blendedCords in nearCoords)
            {
                Group preferredGroup = GetPreferredGroup(blendedCords);
                
                float biomeWeight = blendFlatBiomesLess ? currentBiomeSettings!.heightSpread : 1f;
                float distanceWeight = 1f / ((blendedCords - coords).manhattanDistance + 1f);
                float blendWeight = biomeWeight * distanceWeight;
                
                // dont blend biome with itself. If flat biome non-blending is on, dont blend with anything
                Coords blendAs = preferredGroup == currentBiomeSettings.relevantGroup ? coords : blendFlatBiomesLess ? coords : blendedCords;
                
                totalHeight += GetBiomeHeight(blendAs, groupSettingsByGroup[preferredGroup] as BiomeGroupSettings) * blendWeight;
                totalWeight += blendWeight;
            }

            return (totalHeight / totalWeight).Round();
        }

        /// <summary>
        /// When creating a group, randomly offsets the Perlin noise to avoid symmetry.
        /// </summary>
        protected override GroupSettings CreateDefaultItem(Group relevantGroup)
        {
            BiomeGroupSettings groupSettings = GroupSettings.Init<BiomeGroupSettings>(relevantGroup) as BiomeGroupSettings;
            groupSettings!.randomOffset = Random.Range(10000f, 20000f);
            return groupSettings;
        }

        #endregion

        #region Internal methods

        /// <summary>
        /// Samples perlin noise to obtain "how likely" is a biome to be at coords.
        /// </summary>
        private float GetBiomeValue(Coords coords, BiomeGroupSettings bgs)
        {
            float scale = bgs.size;
            float offset = bgs.randomOffset;
            float rangeMax = bgs.frequency;
            
            return GetPerlinNoise(coords, scale: scale, rangeMax: rangeMax, offset: offset);
        }

        /// <summary>
        /// Samples perlin noise to obtain what would the elevation be if it was the biome bgs.
        /// </summary>
        private float GetBiomeHeight(Coords coords, BiomeGroupSettings bgs)
        {
            float scale = bgs.heightSpread / bgs.ruggedness;
            float rangeMax = bgs.middleElevation + bgs.heightSpread / 2;
            float rangeMin = bgs.middleElevation - bgs.heightSpread / 2;
            float offset = bgs.randomOffset;

            return GetPerlinNoise(coords, scale: scale, rangeMax: rangeMax, rangeMin: rangeMin, offset: offset);
        }

        /// <summary>
        /// Returns a lot or not a lot of coordinates to sample for blending.
        /// </summary>
        private HashSet<Coords> GetSamples(Coords coords)
        {
            if (!fastBlending)
            {
                return tileManager.GetCoordsWithinRange(coords, heightBlendRange);
            }
            
            HashSet<Coords> coordsSet = new HashSet<Coords>(8 * heightBlendRange + 1) {coords};

            for (int i = 1; i <= heightBlendRange; i++)
            for (int j = -1; j <= 1; j += 2)
            for (int k = -1; k <= 1; k += 2)
                coordsSet.Add(coords + new Coords(j, k));

            return coordsSet;
        }

        #endregion
    }
}
