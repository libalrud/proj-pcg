using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace proj_pcg
{
    
    /// <summary>
    /// Class for the Unity serializer to work properly.
    /// </summary>
    [Serializable]
    public class InitialTiles : SerializableDictionary<Coords, GameObject>{}

    /// <summary>
    /// Encompasses the initial state of a tile system.
    /// Featuring several Unity limitation workarounds.
    /// Allows setting initial preferences for the tile states.
    /// </summary>
    [Serializable]
    public class InitialState : TileSystemBase
    {
        /// <summary>
        /// If nonzero, Gizmos will visualize relevant information in the given range.
        /// That is, which tile is selected, and which have a preset state.
        /// </summary>
        [SerializeField] [Header("Editor settings")]
        [Tooltip("Gizmos will visualize relevant information in the given range.")]
        public int showGizmosInRange = 10;
        
        /// <summary>
        /// Delimits which tile group should be at what coordinates.
        /// </summary>
        [FormerlySerializedAs("prefferedTileGroups")]
        [Header("Debug values (read only)")]
        [SerializeField] [Tooltip("Shows preferred initial state data of the given type for each coordinate")]
        protected PreferredTileGroup preferredTileGroups;
        
        /// <summary>
        /// Delimits which tile should be at what coordinates.
        /// </summary>
        [Tooltip("Shows preferred initial state data of the given type for each coordinate.")]
        [FormerlySerializedAs("prefferedTileIDs")] [SerializeField]
        protected PreferredTileID preferredTileIDs;
        
        /// <summary>
        /// Delimits which tile elevation should be at what coordinates.
        /// Currently not supported in editor and oracle
        /// </summary>
        [Tooltip("Shows preferred initial state data of the given type for each coordinate.")]
        [FormerlySerializedAs("prefferedTileElevations")] [SerializeField]
        protected PreferredTileElevation preferredTileElevations;

        /// <summary>
        /// A set of all currently selected coordinates. 
        /// </summary>
        protected HashSet<Coords> selectedCoordsInEditor;
        
        /// <summary>
        /// A dictionary of all currently displayed initial tiles
        /// </summary>
        [SerializeField][HideInInspector][ReadOnly]
        protected InitialTiles showedInitialTiles;

        /// <summary>
        /// Removes initial state tiles before starting.
        /// </summary>
        private void Awake()
        {
            foreach ((_, GameObject showedTile) in showedInitialTiles)
            {
                Destroy(showedTile);
            }
            
            showedInitialTiles.Clear();
        }

        /// <summary>
        /// Shows initial state tiles again.
        /// </summary>
        private void OnDestroy()
        {
            foreach ((var key, int value) in preferredTileIDs)
            {
                UpdateTileDisplayAtCoords(key);
            }
        }

        /// <summary>
        /// Updates variables
        /// </summary>
        private void OnValidate()
        {
            selectedCoordsInEditor ??= new HashSet<Coords>();
            showedInitialTiles ??= new InitialTiles();

            // check render distance
            showGizmosInRange.Clamp(0, 100);
        }

        private void Reset()
        {
            OnValidate();
            ResetAll();
        }

        /// <summary>
        /// True iff ANY information about the tile is preselected.
        /// </summary>
        public bool HasPreferredTileState(Coords coords)
        {
            return preferredTileGroups.ContainsKey(coords) || preferredTileIDs.ContainsKey(coords) ||
                   preferredTileElevations.ContainsKey(coords);
        }

        #if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (showGizmosInRange == 0)
                return;

            UpdateGizmos();
        }

        /// <summary>
        /// Renders gizmos.
        /// </summary>
        public void UpdateGizmos()
        {
            UnityEditor.SceneView sceneView = UnityEditor.SceneView.currentDrawingSceneView;
            if (sceneView == null)
                return;

            Vector3 cameraPosition = sceneView.camera.transform.position;
            HashSet<Coords> nearCoords = tileManager.GetCoordsWithinRange(tileMap.GetNearestTile(cameraPosition), showGizmosInRange);

            foreach (Coords coords in nearCoords)
            {
                // if not already placed, show gizamo
                if (!preferredTileIDs.ContainsKey(coords))
                {
                    ShowGizmoForCoords(coords, tileMap.GetApproximateTileRadius());
                }
                else
                {
                    // if already placed, show real game object
                    if (showedInitialTiles.ContainsKey(coords))
                        ShowGizmoForCoords(coords, tileMap.GetApproximateTileRadius());

                    // UpdateTileDisplayAtCoords(coords);
                }
            }
        }
        #endif

        /// <summary>
        /// For the given coords, instantiates mock tile to show the initial state
        /// </summary>
        private void UpdateTileDisplayAtCoords(Coords coords)
        {
            // if true, tile needs an update
            if (showedInitialTiles.ContainsKey(coords))
            {
                DestroyImmediate(showedInitialTiles[coords]);
                showedInitialTiles.Remove(coords);
            }
            
            // if nothing to be shown, dont do anything (except removing as above)
            if (!preferredTileIDs.ContainsKey(coords))
                return;
            
            TileData tileData = tileManager.mainLayerTileList[preferredTileIDs[coords]];
            
            TileData[] preferredTileLayers = Array.Empty<TileData>();

            TileInstance tileInstance = new TileInstance(tileData, preferredTileLayers,
                coords, tileData.currentElevation);

            GameObject result = tileMap.InstantiateTile(tileInstance);
            showedInitialTiles[coords] = result;
        }

        /// <summary>
        /// Renders a single gizmo
        /// </summary>
        /// <param name="coords">The coordinates of a tile</param>
        /// <param name="approxTileSize">The approximate size of a tile (e.g. minimal distance between coord positions)</param>
        public void ShowGizmoForCoords(Coords coords, float approxTileSize)
        {
            Vector3 tileSize = new Vector3(0.72f, 0.1f, 0.72f) * approxTileSize;
            Color color = new Color(
                0, 
                HasPreferredTileState(coords) ? 0.3f : 0f, 
                selectedCoordsInEditor.Contains(coords) ? 0.3f : 0f, 
                preferredTileIDs.ContainsKey(coords) ? 0.3f : 1f);

            Gizmos.color = color;
            Gizmos.DrawCube(tileMap.GetGlobalPositionOfCoords(coords), tileSize);
        }

        /// <summary>
        /// Set any number of selected coordinates. This is the coordinates that are editable in the UI.
        /// </summary>
        public void SetSelectedCoordinates(IEnumerable<Coords> selectedCoords)
        {
            selectedCoordsInEditor.Clear();
            foreach (var coords in selectedCoords)
            {
                selectedCoordsInEditor.Add(coords);
            }
        }

        /// <summary>
        /// Set a single selected coordinate.
        /// </summary>
        public void SetSelectedCoordinates(Coords coords)
        {
            selectedCoordsInEditor.Clear();
            selectedCoordsInEditor.Add(coords);
        }
        
        /// <summary>
        /// The Reset button calls this and this resets all data.
        /// </summary>
        public void ResetAll()
        {
            foreach ((_, GameObject tileObject) in showedInitialTiles)
            {
                DestroyImmediate(tileObject);
            }
            
            showedInitialTiles.Clear();
            
            preferredTileGroups = new PreferredTileGroup();
            preferredTileIDs = new PreferredTileID();
            preferredTileElevations = new PreferredTileElevation();
        }

        #region boilerplate methods

        /// <summary> True if initial state contains a preferred tile group for the coords.</summary>
        public bool HasTileGroup(Coords coords)
        {
            return preferredTileGroups.ContainsKey(coords);
        }

        /// <summary> True if initial state contains a preferred tileData for the coords.</summary>
        public bool HasTileID(Coords coords) => preferredTileIDs.ContainsKey(coords);
        
        /// <summary> True if initial state contains a preferred tile elevation for the coords.</summary>
        public bool HasTileElev(Coords coords) => preferredTileElevations.ContainsKey(coords);
        
        // /// <summary> True if initial state contains a preferred tile layer definition for the coords.</summary>
        // public bool HasTileLayers(Coords coords) => prefferedTileLayerIDs.ContainsKey(coords);

        /// <summary>
        /// Get the preferred tile group for the given coords. Null if none is set.
        /// </summary>
        public Group GetTileGroup(Coords coords)
        {
            return preferredTileGroups.TryGetValue(coords, out var val) ? val : null;
        }
        
        /// <summary>
        /// Get the preferred tile id for the given coords. -1 if none is set.
        /// </summary>
        public int GetTileID(Coords coords)
        {
            return preferredTileIDs.TryGetValue(coords, out var val) ? val : -1;
        }
        
        /// <summary>
        /// Get the preferred tile elevation for the given coords. 0 if none is set.
        /// </summary>
        public int GetTileElev(Coords coords)
        {
            return preferredTileElevations.TryGetValue(coords, out var val) ? val : 0;
        }

        /// <summary>
        /// Sets preferred tile group FOR EACH SELECTED TILE.
        /// Updates UI.
        /// </summary>
        public void SetTileGroup(Group group)
        {
            foreach (Coords coords in selectedCoordsInEditor)
            {
                preferredTileGroups[coords] = group;
                UpdateTileDisplayAtCoords(coords);
            }
        }

        /// <summary>
        /// Sets preferred tile ID FOR EACH SELECTED TILE.
        /// Updates UI.
        /// </summary>
        public void SetTileID(int id)
        {
            foreach (Coords coords in selectedCoordsInEditor)
            {
                preferredTileIDs[coords] = id;
                UpdateTileDisplayAtCoords(coords);
            }
        }

        /// <summary>
        /// Sets preferred tile elevation FOR EACH SELECTED TILE.
        /// Updates UI.
        /// </summary>
        public void SetTileElevation(int elevation)
        {
            foreach (Coords coords in selectedCoordsInEditor)
            {
                preferredTileElevations[coords] = elevation;
                UpdateTileDisplayAtCoords(coords);
            }
        }

        /// <summary>
        /// Sets a preferred tile group and updates in the view.
        /// </summary>
        public void UnsetTileGroup(Coords coords)
        {
            if (preferredTileGroups.ContainsKey(coords)) 
                preferredTileGroups.Remove(coords);
            
            UpdateTileDisplayAtCoords(coords);
        }

        /// <summary>
        /// Sets a preferred tile ID and updates in the view.
        /// </summary>
        public void UnsetTileID(Coords coords)
        {
            if (preferredTileIDs.ContainsKey(coords)) 
                preferredTileIDs.Remove(coords);
            
            UpdateTileDisplayAtCoords(coords);
        }

        /// <summary>
        /// Sets a preferred tile elevation and updates in the view.
        /// </summary>
        public void UnsetTileElevation(Coords coords)
        {
            if (preferredTileElevations.ContainsKey(coords)) 
                preferredTileElevations.Remove(coords);

            UpdateTileDisplayAtCoords(coords);
        }

        #endregion
        #region unity inspector stuff

        /// <summary>
        /// Unity-inspector workaround class
        /// </summary>
        [Serializable]
        public class PreferredTileGroup : SerializableDictionary<Coords, Group>{}

        /// <summary>
        /// Unity-inspector workaround class
        /// </summary>
        [Serializable]
        public class PreferredTileID : SerializableDictionary<Coords, int> {}
    
        /// <summary>
        /// Unity-inspector workaround class
        /// </summary>
        [Serializable]
        public class PreferredTileElevation : SerializableDictionary<Coords, int>{}
        
        #endregion

    }
    

    
    
}
