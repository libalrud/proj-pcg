using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace proj_pcg
{
    /// <summary>
    /// The most advanced Oracle based on the AC-3 CSP algorithm.
    /// </summary>
    public abstract class AC3ConnectivityOracle : Oracle
    {

        #region Editor variables

        /// <summary>
        /// Collision reduction check distance.
        /// </summary>
        [SerializeField] [Tooltip("Collision reduction check distance. " +
                                  "Higher = less conflicts, slower calculations")]
        private int checkDistance = 10;

        /// <summary>
        /// Collision reduction check algorithm iterations.
        /// </summary>
        [SerializeField] [Tooltip("Collision reduction iteration cutoff. Higher = " +
                                        "less conflicts, slower calculations")]
        private int checkIterations = 100;
        
        /// <summary>
        /// A factor of 1 will make Oracle disregard elevation mismatch from InitialState.
        /// A factor of 10 means tiles offset by 1 are less likely
        /// </summary>
        [SerializeField] [Tooltip("A factor of 1 will make Oracle disregard elevation mismatch from " +
                                        "InitialState. A factor of 10 means tiles offset by 1 are less likely")]
        private float elevationMismatchPenalty = 10;

        #endregion 
        #region Internal Variables
        
        private Dictionary<Coords, Possibility> possibilities;

        #endregion
        #region Override methods

        private void Awake()
        {
            possibilities ??= new Dictionary<Coords, Possibility>();
        }

        private void OnValidate()
        {
            if (elevationMismatchPenalty < 0)
                elevationMismatchPenalty = 0;
            
            if (checkDistance < 0)
                checkDistance = 0;
            
            if (checkIterations < 0)
                checkIterations = 0;
            
        }

        // handles
        // - initial state
        // - height map: biomes and elevation
        // - possibility connectivity
        protected override Dictionary<TileData, float> SelectRelevantTiles(
            Dictionary<TileData, float> candidates, Coords coords, Layer tiles)
        {
            List<TileData> candidateKeys = candidates.Keys.ToList();
            
            if (HasPreferredTile(coords))
            {
                // is already accounted for
                TileData preferredTile = PreferredTile(coords, tiles);
                foreach (TileData tile in candidateKeys)
                {
                    if (tile.id != preferredTile.id)
                        candidates.Remove(tile);
                }

                return candidates;
            }

            // tiles outside of group will only get selected if they are the only ones compatible
            Group preferredGroup = PreferredGroup(coords);
            foreach (TileData tile in candidateKeys)
            {
                if (tile.group != preferredGroup) 
                    candidates[tile] = 0f;
            }

            // decreases fast
            int preferredElevation = PreferredElevation(coords);
            foreach (TileData tile in candidateKeys)
            {
                candidates[tile] *= ElevationMismatchPenalty(tile.currentElevation, preferredElevation);
            }
            
            // possibility should already represent which values are not compatible.
            return GetPossibility(coords, tiles).MultiplyWeights(candidates);
        }

        public override void OnTileGenerated(TileInstance tileInstance, Layer tiles)
        {
            if (checkDistance == 0)
                return;

            Coords coords = tileInstance.coords;
            
            possibilities[coords] = new Possibility(tiles, tileInstance.mainTileData);

            PreparePossibilities(tiles, coords);
            
            AC3(tiles, coords);
        }
        
        /// <summary>
        /// Return how much should an elevation be considered favourable given an ideal state.
        /// Should usually return 1 for an exact match and decrease thereafter.
        /// </summary>
        protected virtual float ElevationMismatchPenalty(int currentElevation, int idealElevation)
        {
            int difference = Mathf.Abs(currentElevation - idealElevation);
            return 1f / Mathf.Pow((elevationMismatchPenalty + 1), difference);
        }

        #endregion
        #region Internal methods
        
        

        
        /// <summary>
        /// Possibilities need to be initialized before AC-3 runs.
        /// Unfortunately, initialization of some Possibilities requires AC-3 itself.
        /// This solves all related problems by recursively expanding initialized Possibilities and doing the AC-3
        /// </summary>
        private void PreparePossibilities(Layer tiles, Coords coords)
        {
            foreach (Coords nearCoords in tileManager.GetCoordsWithinRange(coords, checkDistance))
            {
                if (possibilities.ContainsKey(nearCoords)) 
                    continue;

                if (HasPreferredTile(nearCoords))
                {
                    possibilities[nearCoords] = new Possibility(tiles, PreferredTile(nearCoords, tiles));
                    PreparePossibilities(tiles, nearCoords);
                    AC3(tiles, nearCoords);
                }
                else if (initialState.HasTileGroup(nearCoords))
                {
                    possibilities[nearCoords] = new Possibility(tiles, initialState.GetTileGroup(nearCoords));
                    PreparePossibilities(tiles, nearCoords);
                    AC3(tiles, nearCoords);
                }
                else
                {
                    possibilities[nearCoords] = new Possibility(tiles);
                }
            }
        }
        
        /// <summary>
        /// Does the AC3
        /// </summary>
        private void AC3(Layer tiles, Coords coords)
        {
            Coords centerCoords = coords.Copy();
            int iterations = 0;
            
            // 2) add new tile coords to queue
            var AC3queue = new Queue<Coords>();
            AC3queue.Enqueue(coords); // tile at coords has been modified. E.g., add to queue
        
            // 3) run the arc consistency 3 algorithm until queue is empty or limit is reached
            // only within calculation range from tile tho
        
            while (AC3queue.Count != 0 && iterations++ < checkIterations) 
            {
                Coords currentCoords = AC3queue.Dequeue();
                Possibility currentPossibility = possibilities[currentCoords];

                // too far from original tile
                if ((centerCoords - currentCoords).manhattanDistance >= checkDistance)
                    continue;

                // check consistency with all neighbor's Possibilities
                // result is either 1 or 0 for each neighbor's each tile candidate
                // option decreased -> enqueue
                foreach ((int neighborDirection, Coords neighborCoords) 
                         in tileMap.GetNeighboringPositions(currentCoords))
                {
                    // check if options can be limited using Compatibilities
                    // if something was limited, add both(?) tiles to queue.
                    // limit by distance and/or total number of iterations
                    // Or, I'd say, keep debug tile for now and wait until later to find better solution for conflict resolution
                    
                    // example:
                    // Possibility[current]  = [0,0,0,0,...,1]
                    // Possibility[neighbor] = [0,1,1,0,.....]
                    // new Possibility       = [0,0,1,0,.....]
                    // some values were removed   ^
        
                    Possibility neigborPossibility = possibilities[neighborCoords];
        
                    // dont affect existing or conflicted tiles
                    if (neigborPossibility.exists || neigborPossibility.isConflicted)
                        continue;
        
                    if (AnyOptionDeleted(tiles, neighborDirection, currentPossibility, neigborPossibility))
                    {
                        AC3queue.Enqueue(neighborCoords);
                        AC3queue.Enqueue(currentCoords);
                    }
                }
                
            }
        }
        
        /// <summary>
        /// https://cw.fel.cvut.cz/wiki/_media/courses/zui/slides-l9-2023.pdf, page 15
        /// </summary>
        private bool AnyOptionDeleted(Layer tiles, int neighborDirection, 
            Possibility currentPossibility, Possibility neigborPossibility)
        {
            bool anyOptionDeleted = false;
            foreach (TileData potentialNeighborTile in tiles)
            {
                // skip tiles outside of domain
                if (neigborPossibility.GetValue(potentialNeighborTile) == false || neigborPossibility.isConflicted)
                    continue;

                bool supported = false;
                foreach (TileData potentialCurrentTile in tiles)
                {
                    // skip tiles outside of domain
                    if (currentPossibility.GetValue(potentialCurrentTile) == false || currentPossibility.isConflicted)
                        continue;

                    // for each potential neighbor tile, check if there is any current tile supporting it
                    // must be mutually compatible with at lest one tile1 option
                    // if any value was removed, add to queue
                    
                    bool isCompatible = GetCompatibility(potentialCurrentTile, potentialNeighborTile, neighborDirection) > 0;

                    if (isCompatible)
                    {
                        supported = true;
                    }
                }

                // option not supported by any tile
                if (!supported)
                {
                    neigborPossibility.RemoveValue(potentialNeighborTile);
                    anyOptionDeleted = true;
                }
            }

            return anyOptionDeleted;
        }

        /// <summary>
        /// True if Initial state defines a preferred tile ID
        /// </summary>
        private bool HasPreferredTile(Coords coords)
        {
            return initialState.HasTileID(coords);
        }

        /// <summary>
        /// Returns the preferred tile to be placed.
        /// </summary>
        private TileData PreferredTile(Coords coords, Layer tiles)
        {
            return tiles[initialState.GetTileID(coords)];
        }


        /// <summary>
        /// Returns the preferred group of tiles to be chosen from.
        /// </summary>
        private Group PreferredGroup(Coords coords)
        {
            if (initialState.HasTileGroup(coords))
            {
                return initialState.GetTileGroup(coords);
            }
            else
            {
                return terrainMap.GetPreferredGroup(coords);
            }
        }


        /// <summary>
        /// Returns the preferred elevation at a coordinate.
        /// </summary>
        private int PreferredElevation(Coords coords)
        {
            if (initialState.HasTileElev(coords))
                return initialState.GetTileElev(coords);
            else
            {
                return terrainMap.GetPrefferedHeight(coords);
            }
        }

        /// <summary>
        /// Finds the relevant Possibility or creates one if it doesn't exist yet.
        /// </summary>
        private Possibility GetPossibility(Coords coords, Layer tiles)
        {
            // this happens typically when coords is far off from any already known ones
            if (!possibilities.ContainsKey(coords))
            {
                possibilities[coords] = new Possibility(tiles);
            }

            return possibilities[coords];
        }

        #endregion
        #region Data definitions

        /// <summary>
        /// Unlike the prototype version, this is much more optimised,
        /// with less overhead on creation but more on access.
        /// </summary>
        [Serializable]
        private class Possibility
        {
            /// <summary>
            /// A float for each tile ID on the main layer.
            /// </summary>
            [SerializeField] [HideInInspector]
            private bool[] values;
            
            /// <summary>
            /// True if this tile has been selected by the oracle already.
            /// </summary>
            [SerializeField] [HideInInspector]
            public bool exists;

            /// <summary>
            /// True if no tile can be placed in this spot, e.g. all tiles have a value of false
            /// </summary>
            public bool isConflicted => !values.Any();

            /// <summary>
            /// If only one option is selectable; 
            /// </summary>
            public bool onlyOneRealOption => values.Count(value => value) == 1;
            
            /// <summary>
            /// Create a Possibility assigning a float value to each tile
            /// that could be placed at the given coordinates.
            /// </summary>
            /// <param name="tiles">A list of all tile options</param>
            public Possibility(Layer tiles)
            {
                values = new bool[tiles.Count];
                
                foreach (TileData tile in tiles)
                {
                    values[tile.id] = true;
                }

                exists = false;
            }

            /// <summary>
            /// For tiles that have just been placed, or tiles that have an initial state.
            /// </summary>
            /// <param name="tiles">A list of all tile options</param>
            /// <param name="selectedTile">The tile at the given coordinates</param>
            public Possibility(Layer tiles, TileData selectedTile) : this(tiles)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    // override any values to 0 or 1 based on if they
                    // represent the actually placed value or not
                    values[i] = selectedTile.id == i;
                }

                exists = true;
            }

            /// <summary>
            /// For tiles that have an initial state group.
            /// Otherwise same as Possibility(Layer, TileData)
            /// </summary>
            public Possibility(Layer tiles, Group selectedGroup) : this(tiles)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    // override any values to 0 or 1 based on if they
                    // represent the actually placed value or not
                    values[i] = selectedGroup.groupName == tiles[i].group.groupName;
                }

                exists = true;
            }

            /// <summary>
            /// Returns the currect possibility for the given TileData
            /// </summary>
            public bool GetValue(TileData tileData)
            {
                return values[tileData.id];
            }

            /// <summary>
            /// Returns the currect possibility for the given TileData
            /// </summary>
            public bool GetValue(int tileID)
            {
                return values[tileID];
            }

            /// <summary>
            /// Sets the currect possibility for the given TileData to false
            /// </summary>
            public void RemoveValue(TileData tileData)
            {
                RemoveValue(tileData.id);
            }

            /// <summary>
            /// Sets the currect possibility for the given TileData to false
            /// </summary>
            public void RemoveValue(int tileDataID)
            {
                if (exists)
                    throw new Exception("Possibility already represents an existing value!");
                
                values[tileDataID] = false;
            }

            /// <summary>
            /// True if tile is compatible and hasnt been already placed
            /// False if tile already is placed or if false
            /// </summary>
            public bool IsCompatible(TileData tileData)
            {
                return IsCompatible(tileData.id);
            }

            /// <summary>
            /// True if tile is compatible and hasnt been already placed
            /// False if tile already is placed or if false
            /// </summary>
            public bool IsCompatible(int id)
            {
                if (exists)
                    return false;

                return values[id];
            }

            /// <summary>
            /// Behaves like Dictionary.MultiplyWeights in a way;
            /// removes incompatible variables from dict
            /// </summary>
            public Dictionary<TileData, float> MultiplyWeights(Dictionary<TileData, float> dict)
            {
                foreach (TileData tile in dict.Keys.ToList())
                {
                    if (GetValue(tile) == false)
                    {
                        dict.Remove(tile);
                    }
                }
            
                return dict;
            }
        }

        #endregion
    }
}
