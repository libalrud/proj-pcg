using System.Collections.Generic;

namespace proj_pcg
{
    /// <summary>
    /// The simplest possible oracle. Basically equivalent to AC3 oracle with a radius of 1
    /// </summary>
    public abstract class BasicConnectivityOracle : Oracle
    {
    
        protected override Dictionary<TileData, float> SelectRelevantTiles(Dictionary<TileData, float> candidates, Coords coords, Layer tiles)
        {
            return candidates.MultiplyWeights(GetCompatibilitiesForLayer(tileManager.mainLayerTileList, coords));
        }

        public override void OnTileGenerated(TileInstance tileInstance, Layer tiles) { }
    }
}
