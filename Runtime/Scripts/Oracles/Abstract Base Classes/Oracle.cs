using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace proj_pcg
{
    /// <summary>
    /// OracleBase contains methods common to all Oracles, whether they be Chessboard-like or connectivity based.
    /// In practice, the standard oracle will be the best due to alignment with InitialState and HeightMap.
    /// </summary>
    public abstract class Oracle : TileSystemBase
    {
        /// <summary>
        /// Expresses a soft zero in cases where normal zero is a hard zero.
        /// (The hard zero is usually expressed by omitting the value)
        /// </summary>
        protected const float softZero = 1.175494e-38f * 10f;

        /// <summary>
        /// This can be used in case of conflicts etc.
        /// Has an ID of -1 and is compatible with all other tiles.
        /// </summary>
        [SerializeField] [HideInInspector]
        protected TileData debugTile;

        /// <summary>
        /// Used to fill in empty layers.
        /// </summary>
        [SerializeField] [HideInInspector]
        protected TileData emptyTileLayer;
        

        #region Editor variables

        /// <summary>
        /// What to do if tile frequency is not proportional to effective tile weight. 
        /// </summary>
        [SerializeField] [Tooltip("What to do if tile frequency is not proportional to tile weight. " +
                                        "False = nothing, True = try to offset weight arbitrarily. " +
                                        "This can lead to more conflicts.")]
        public bool tileFrequencyCompensation = false;

        /// <summary>
        /// False - do nothing, True - decrease tile weight when generating multiple rotations
        /// </summary>
        [SerializeField] [Tooltip("False - do nothing, True - decrease tile weight when generating multiple rotations")]
        public bool tileRotationCompensation = true;

        #endregion
        #region Internal variables

        /// <summary>
        /// A value between 0 and 1 detailing how much are two main layer tiles compatible in a certain direction
        /// </summary>
        private float[][,,] tileCompatibility;

        
        /// <summary>
        /// A value between 0 and 1 for how much two tiles from different layers are compatible
        /// </summary>
        private float[][,] interLayerCompatibilities;

        protected float sumOfTileWeights;

        #endregion

        #region Unity event functions

        private void Start()
        {
            if (debugTile == null)
                return;
            
            if (debugTile.id != -1) 
                debugTile = debugTile.InstantiateAndRotate(-1, 0, -1000, new Group("debug"));
            
            sumOfTileWeights = tileManager.mainLayerTileList.Sum(
                    t => tileRotationCompensation 
                    ? t.weight / t.uniqueRotations
                    : t.weight);
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Calculates border compatibility in all and between all layers.
        /// To be called after tile types are loaded.
        /// </summary>
        /// <param name="mainTiles">The main tile Layer</param>
        /// <param name="layerTiles">Tiles on all the other layers</param>
        public void CalculateCompatibilities(Layer mainTiles, List<Layer> layerTiles)
        {
            int additionalLayerCount = layerTiles.Count;
            
            tileCompatibility = new float[additionalLayerCount + 1][,,];
            interLayerCompatibilities = new float[additionalLayerCount][,];

            tileCompatibility[0] = IntraLayerCompatibility(mainTiles);
            for (int i = 0; i < additionalLayerCount; i++)
            {
                // layer 1 has ID 1. Layer zero is the main layer.
                tileCompatibility[i + 1] = IntraLayerCompatibility(layerTiles[i]);
                interLayerCompatibilities[i] = InterLayerCompatibility(mainTiles, layerTiles[i]);
            }
        }

        /// <summary>
        /// Chooses a tile to be placed at some coordinates. Instantiates it.
        /// </summary>
        /// <param name="mainTiles">The main tile layer</param>
        /// <param name="otherTiles">Additional tile layers</param>
        /// <param name="coords">Coordinates to generate at</param>
        /// <returns>A TileInstance of the tile at a certain elevation, including tile layers.</returns>
        public TileInstance GenerateTileAtCoords(Layer mainTiles, List<Layer> otherTiles, Coords coords)
        {
            Dictionary<TileData, float> candidates = GetCandidates(mainTiles);

            // tile frequency comp is "slapped on top" of calculation
            // only for main layer tiles
            if (tileFrequencyCompensation)
            {
                foreach (TileData tile in candidates.Keys.ToList())
                {
                    candidates[tile] = GetCompensatedTileWeight(tile);
                }
            }

            // this is the main part of the calculation
            // the following should result in hard zeroes:
            // tile compatibility
            // the following should never result in "hard zeroes":
            // initial state preferrences
            // height map group and height preferences
            // tile connectivity and elevation compatibility
            candidates = SelectRelevantTiles(candidates, coords, mainTiles);

            
            TileData mainTile;
            try
            {
                mainTile = candidates.WeightedRandomChoice(true);
            }
            // if no tile could be selected
            catch (InvalidOperationException)
            {
                mainTile = GenerateTileAtConflict(mainTiles, coords, tileManager.GetNeighboringTilesLayers(coords));
            }

            // choose layer tiles (can be null!)
            TileData[] layers = otherTiles.Select(layer => SelectTileForLayer(layer, coords, mainTile)).ToArray();
            
            TileInstance chosenOne = new TileInstance(mainTile, layers, coords, mainTile.currentElevation);

            // conflict reduction
            
            OnTileGenerated(chosenOne, mainTiles);

            return chosenOne;
        }

        #endregion

        #region Abstract methods
        
        /// <summary>
        /// Calculate how compatible are two borders, whether vertical or horizontal.
        /// </summary>
        protected abstract float CalculateBorderCompatibility(BorderData border1, BorderData border2);

        /// <summary>
        /// Choose which tile (either main layer or other layer) to place when
        /// no tile can be placed
        /// </summary>
        /// <returns>Any tile deemed "good enough" to be placed there.</returns>
        protected abstract TileData GenerateTileAtConflict(Layer fullLayer, Coords coords, Dictionary<int, TileData> neighbors);

        protected abstract float GetCompensatedTileWeight(TileData tile);
        
        /// <summary>
        /// Selects a "good" tile (i.e. tile's main layer) from a preset selection.
        /// </summary>
        /// <param name="candidates">Tiles with associates weights. Some layer's tiles may be missing due to being
        /// hard-zero incompatible.</param>
        /// <param name="coords">The coordinates to generate at.</param>
        /// <param name="tiles">The complete layer to select from (including incompatible options)</param>
        /// <returns>A final weight distribution for available tiles. Zero-weight tiles can still be chosen if
        /// all other tiles are zero-weight. This is a "soft zero". If a tile should not be selected, it
        /// should be removed from the dictionary entirely.</returns>
        protected abstract Dictionary<TileData, float> SelectRelevantTiles(Dictionary<TileData, float> candidates, Coords coords, Layer tiles);

        /// <summary>
        /// Defines what to do when a tile has been generated.
        /// Called when tile generated or when tile loaded from save state.
        /// </summary>
        /// <param name="tileInstance">The newly generated tile instance.</param>
        /// <param name="tiles">The main layer</param>
        public abstract void OnTileGenerated(TileInstance tileInstance, Layer tiles);

        #endregion

        #region Internal methods
        
        /// <summary>
        /// Calculates a list of relevant values, e.g. candidates, from a Layer.
        /// Candidates with zero weight are removed (hard zero).
        /// Candidates with n rotations have their weight decreased n times iff tileRotationCompensation is true.
        /// </summary>
        /// <param name="currentLayer">The layer to calculate from</param>
        /// <returns></returns>
        protected Dictionary<TileData, float> GetCandidates(Layer currentLayer)
        {
            Dictionary<TileData, float> candidates = currentLayer.ToDictionary(
                tileData => tileData,
                tileData => tileData.weight / (tileRotationCompensation ? tileData.uniqueRotations : 1)
            );
            
            return candidates;
        }

        /// <summary>
        /// Returns which tiles are compatible with the current tile's neighbors.
        /// Includes vertical compatibility for main layer.
        /// If result is zero, value represents "soft zero". If value is not preset, it represents "hard zero".
        /// </summary>
        /// <param name="currentLayer">The layer to calculate candidate compatibilities from</param>
        /// <param name="coords">The coordinates of the tile to be placed</param>
        /// <param name="mainLayerTile">If generating for a tile, add the main tile as a parameter</param>
        /// <returns>For each tile, the amount of compatibility the tile would have if placed at the coords.
        /// 0 represents a value that is too low to fit into float (soft zero).</returns>
        protected Dictionary<TileData, float> GetCompatibilitiesForLayer(Layer currentLayer, Coords coords, TileData mainLayerTile = null)
        {
            if (currentLayer.Count == 0)
                return new Dictionary<TileData, float>();

            int layerID = currentLayer[0].group.layerID;

            Dictionary<TileData, float> candidates = new();
            Dictionary<int,TileData> neighbors = tileManager.GetNeighboringTilesLayers(coords, layerID);

            foreach (TileData tileData in currentLayer)
            {
                float compatibility = 1f;

                // for vertical border, if any relevant
                if (layerID > 0 && mainLayerTile != null)
                {
                    float mainTileCompatibility = GetLayerCompatibility(mainLayerTile, tileData);
                    
                    if (compatibility > 0 && mainTileCompatibility > 0 && compatibility * mainTileCompatibility == 0)
                        compatibility = softZero;
                    else
                        compatibility *= mainTileCompatibility;
                }

                // for each horizontal border
                foreach ((int direction, TileData neighbor) in neighbors)
                {
                    float neighborCompatibility = GetCompatibility(tileData, neighbor, direction);

                    // avoid floating point limitations
                    if (compatibility > 0 && neighborCompatibility > 0 && compatibility * neighborCompatibility == 0)
                        compatibility = softZero;
                    else
                        compatibility *= neighborCompatibility;
                }

                if (compatibility is > 0 and < softZero)
                    candidates[tileData] = 0f;
                
                else if (compatibility > 0) 
                    candidates[tileData] = compatibility;
            }

            return candidates;
        }

        
        /// <summary>
        /// Chooses tile layers for a newly selected tile.
        /// Layers have no conflict reduction.
        /// Each layer's tile is selected only by matching the near borders.
        /// May return null.
        /// </summary>
        private TileData SelectTileForLayer(Layer currentLayer, Coords coords, TileData mainTile)
        {   
            if (currentLayer.Count == 0)
                return null;
            
            Dictionary<TileData, float> candidates = GetCandidates(currentLayer);
            Dictionary<TileData, float> compatibleTiles = GetCompatibilitiesForLayer(currentLayer, coords, mainTile);

            candidates = candidates.MultiplyWeights(compatibleTiles);
            
            // if no tile is compatible with base tile
            if (candidates.Count == 0)
                return null;

            // most options now have weight zero. If conflict, all have weight zero. Choose what to do.
            try
            {
                // false, want to select most compatible option
                return candidates.WeightedRandomChoice(false); 
            }
            // if no tile could be selected
            catch (InvalidOperationException)
            {
                return GenerateTileAtConflict(currentLayer, coords, 
                    tileManager.GetNeighboringTilesLayers(coords, currentLayer[0].group.layerID));
            }
        }
        
        /// <summary>
        /// Calculates tile compatibilities for each tile and direction within one layer.
        /// </summary>
        /// <param name="layer">The layer to calculate compatibilities for</param>
        /// <returns>An array with a value for each combination of tile ID"s and direction
        /// for the given layer</returns>
        private float[,,] IntraLayerCompatibility(Layer layer)
        {
            float[,,] compatibility = new float[layer.Count, tileMap.totalTileRotations, layer.Count];

            foreach (TileData tile1 in layer)
            {
                foreach (TileData tile2 in layer)
                {
                    for (int directionFrom1To2 = 0; directionFrom1To2 < tile2.totalRotations; directionFrom1To2++)
                    {
                        int directionFrom2To1 = tileMap.OppositeSide(directionFrom1To2);

                        compatibility[tile1.id, directionFrom1To2, tile2.id] = 
                            HorizontalBorderCompatibility(tile1, directionFrom1To2, tile2, directionFrom2To1);
                    }
                }
            }

            return compatibility;
        }
        
        /// <summary>
        /// Calculates the relative compatibility of two tiles next to each other. Will be cached for future reuse.
        /// </summary>
        private float HorizontalBorderCompatibility(TileData tile1, int directionFrom1To2,
            TileData tile2, int directionFrom2To1)
        {
            if (!ElevationCompatibility(tile1, directionFrom1To2, tile2, directionFrom2To1))
                return 0;

            BorderData nearBorder1 = tile1.sideBorders[directionFrom1To2].borderData;
            BorderData nearBorder2 = tile2.sideBorders[directionFrom2To1].borderData;
            
            return CalculateBorderCompatibility(nearBorder1, nearBorder2);
        }

        /// <summary>
        /// Calculates how compatible two tiles are based on their height profiles.
        /// </summary>
        /// <param name="tile1">The first tile</param>
        /// <param name="directionFrom1To2">Direction from the first tile to the second tile</param>
        /// <param name="tile2">The second tile</param>
        /// <param name="directionFrom2To1">Direction from the second tile to the first tile (redundant, for simplicity)</param>
        /// <returns>Typically 1 if the elevation aligns and 0 if it doesn't</returns>
        private bool ElevationCompatibility(TileData tile1, int directionFrom1To2, 
            TileData tile2, int directionFrom2To1)
        {
            return tile1.currentElevation + tile1.GetBorder(directionFrom1To2).relativeElevation ==
                   tile2.currentElevation + tile2.GetBorder(directionFrom2To1).relativeElevation;
        }

        /// <summary>
        /// Calculates tile compatibilities between two layers
        /// </summary>
        /// <param name="mainTiles">The main layer</param>
        /// <param name="layerTiles">The additional layer</param>
        /// <returns>An array with a value for each combination of tile
        /// IDs for the given layers</returns>
        private float[,] InterLayerCompatibility(Layer mainTiles, Layer layerTiles)
        {
            float[,] compatibility = new float[mainTiles.Count, layerTiles.Count];

            foreach (TileData mainTile in mainTiles)
            {
                foreach (TileData layerTile in layerTiles)
                {
                    compatibility[mainTile.id, layerTile.id] =
                        VerticalBorderCompatibility(mainTile, layerTile);
                }
            }

            return compatibility;
        }
        
        /// <summary>
        /// Calculates vertical border compatibility for each tile combination between the main layer and another layer.
        /// </summary>
        /// <param name="mainTile">A tile from the main layer</param>
        /// <param name="otherTile">A tile from another layer</param>
        /// <returns>A value between 0 and 1 representing how much are these tiles compatible together</returns>
        private float VerticalBorderCompatibility(TileData mainTile, TileData otherTile)
        {
            // null - not compatible with anything
            if (mainTile.verticalBorder.vertBorderData == null)
            {
                return 0f;
            }

            // null - not compatible with anything
            if (otherTile.verticalBorder.vertBorderData == null)
            {
                Debug.LogWarning($"Additional layer tile {otherTile} will never appear " +
                                         "because it has no vertical border definition.");
                return 0f;
            }

            // tile layers must have the same rotation as the main tile
            // or, if the main tile is symmetrical, can have certain other rotations
            int mainTileRotation = otherTile.currentRotation % mainTile.uniqueRotations;
            int otherTileRotation = mainTile.currentRotation % otherTile.uniqueRotations;
            if (mainTileRotation != otherTileRotation)
            {
                return 0f;
            }

            var border1 = mainTile.verticalBorder.vertBorderData;
            var border2 = otherTile.verticalBorder.vertBorderData;
                
            return CalculateBorderCompatibility(border1, border2);
        }

        /// <summary>
        /// Compatibility of two tiles on any layer. 
        /// </summary>
        /// <param name="tile1">Any tile</param>
        /// <param name="tile2">Any tile on the same layer</param>
        /// <param name="directionFrom1to2">The direction from tile 1 to tile 2</param>
        /// <returns>1 if any tile is a debug tile;
        /// else the precalculated compatibility of their borders</returns>
        protected float GetCompatibility(TileData tile1, TileData tile2, int directionFrom1to2)
        {
            if (tile1.id == -1 || tile2.id == -1)
                return 1;

            return tileCompatibility[tile1.group.layerID][tile1.id, directionFrom1to2, tile2.id];
        }

        /// <summary>
        /// Compatibility of two tiles on any layer.
        /// </summary>
        /// <param name="layer">The layer the IDs are relevant for</param>
        /// <param name="tile1">Any tile's ID</param>
        /// <param name="tile2">Any tile's ID for a tile on the same layer</param>
        /// <param name="directionFrom1to2">The direction from tile 1 to tile 2</param>
        /// <returns>1 if any tile is a debug tile; else the precalculated compatibility of their borders</returns>
        protected float GetCompatibility(int layer, int tile1, int tile2, int directionFrom1to2)
        {
            if (tile1 == -1 || tile2 == -1)
                return 1;

            return tileCompatibility[layer][ tile1, directionFrom1to2, tile2];
        }

        /// <summary>
        /// Compatibility between tile and layer tile
        /// </summary>
        /// <param name="mainTile">Any main layer tile</param>
        /// <param name="layerTile">Any layer layer tile</param>
        /// <returns>How much they are compatible</returns>
        protected float GetLayerCompatibility(TileData mainTile, TileData layerTile)
        {
            if (mainTile.id == -1 || layerTile.id == -1)
                return 0;

            
            // index 0 is to compare between layers 0 and 1.
            return interLayerCompatibilities[layerTile.group.layerID - 1] [mainTile.id, layerTile.id];
        }

        #endregion
    }
}
