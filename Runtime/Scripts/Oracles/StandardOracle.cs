﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace proj_pcg
{
    /// <summary>
    /// A standard-behaving Oracle with default responses for conflicts
    /// and default border compatibility behavior.
    /// </summary>
    public class StandardOracle : AC3ConnectivityOracle
    {

        /// <summary>
        /// If false, conflicts will not be reported as warnings.
        /// </summary>
        [Tooltip("If false, conflicts will not be reported as warnings.")]
        public bool reportConflicts = false;
        
        /// <summary>
        /// Calculates which borders are how much compatible
        /// </summary>
        protected override float CalculateBorderCompatibility(BorderData border1, BorderData border2)
        {
            if (border1 == null || border2 == null)
                return 0f;
            
            Dictionary<string, float> left = border1.borders.ToDictionary(bdp => bdp.border.borderName, bdp => bdp.weight);
            Dictionary<string, float> right = border2.borders.ToDictionary(bdp => bdp.border.borderName, bdp => bdp.weight);

            string leftName = border1.borderName;
            string rightName = border2.borderName;
            
            // weight of a name is how much is leftName in right
            float leftWeight = right.Keys.Contains(leftName) ? right[leftName] : -1;
            float rightWeight = left.Keys.Contains(rightName) ? left[rightName] : -1;

            return ((leftWeight + rightWeight) / 2).Clamp(0, 1);
        }

        /// <summary>
        /// Just select any tile that is compatible with most near tiles.
        /// Typically which tile is "least incompatible" with existing neighbors. All tiles are incompatible.
        /// </summary>
        protected override TileData GenerateTileAtConflict(Layer fullLayer, Coords coords, Dictionary<int, TileData> neighbors)
        {
            
            // just select tile that is compatible with most neightbors
            Dictionary<TileData, float> validConnections = fullLayer.ToDictionary(tileData => tileData, tileData =>
            {
                IEnumerable<bool> combIsValid = neighbors.Select(
                    kvp => GetCompatibility(tileData, kvp.Value, kvp.Key) > 0);
                float value = combIsValid.Sum(Convert.ToInt32);
                return value + Random.Range(0, 0.1f);
            });

            // TileData result = validConnections.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;
            TileData result = validConnections.MaxKey();
            
            if (reportConflicts) 
                Debug.LogWarning($"Conflict at {coords}!");

            return result;
        }

        /// <summary>
        /// Calculates the compensation when tiles' weights are compensated.
        /// </summary>
        /// <param name="tile">The tile in question</param>
        /// <returns>Its modified weight.</returns>
        protected override float GetCompensatedTileWeight(TileData tile)
        {
            float t = tileStats.stats.totalTilesGenerated;
            float r = tileStats.stats.tileTypesGenerated[tile.id];

            float preal = r == 0 ? 0 : r / t;
            float pideal = tile.weight / sumOfTileWeights;

            float weightModifier = pideal / preal;
            return tile.weight * weightModifier;
        }
    }
}