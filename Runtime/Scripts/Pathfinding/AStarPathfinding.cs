using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

namespace proj_pcg
{
    /// <summary>
    /// Pathfinding contains a single public method for calculating the shortest path between two points.
    /// The fields represent variables that usually don't change beteween calculations, e.g. settings.
    /// </summary>
    public class AStarPathfinding : Pathfinding
    {
        /// <summary>
        /// The maximum A* iterations before giving up search.
        /// </summary>
        [Tooltip("The maximum A* iterations before giving up search.")]
        public int maximumSteps = 1000;

        /// <summary>
        /// The maximum height difference moving upwards. Has to be higher than maxHeightDown.
        /// </summary>
        [Tooltip("The maximum height difference moving upwards. Has to be higher than maxHeightDown.")]
        public int maxHeightUp = +1;

        /// <summary>
        /// The maximum height difference moving downwards. Has to be lower than maxHeightUp.
        /// </summary>
        [Tooltip("The maximum height difference moving downwards. Has to be lower than maxHeightUp.")]
        public int maxHeightDown = -1;

        /// <summary>
        /// If true, not-yet-generated tiles will be considered traversible to and from.
        /// Else, non-generated tiles will be considered always impassable.
        /// </summary>
        [FormerlySerializedAs("nonGeneratedIsTraversible")] 
        [Tooltip("If true, not-yet-generated tiles will be considered traversable to and from. " +
                       "Else, non-generated tiles will be considered always impassable.")]
        public bool nonGeneratedIsTraversable = false;

        /// <summary>
        /// Calculates and returns a shortest path denoted as a list of traversed tile coordinates.
        /// Its length is the length of the list.
        /// </summary>
        /// <param name="start">The starting coordinate</param>
        /// <param name="end">The goal coordinate</param>
        /// <returns>A list of traversed tile coordinates, or null if no path was found.</returns>
        public override List<Coords> GetPath(Coords start, Coords end)
        {
            return AStar(start, end)?.path;
        }

        /// <summary>
        /// Ideally, an admissible heuristic function for the path distance between current_pos and goal_pos.
        /// Genrally, any function that usually decreases with distance to goal. 
        /// </summary>
        protected virtual float Heuristic(Coords current_pos, Coords goal_pos)
        {
            return (current_pos - goal_pos).manhattanDistance;
        }
    
        /// <summary>
        /// Performs an A* search across loaded tiles. Tiles may be inactive.
        /// Gives up after the specified number of iterations.
        /// </summary>
        protected PQElement AStar(Coords start, Coords goal)
        {
            var visitedCoordsSet = new HashSet<Coords>();
            

            var pq = new CloudCanards.Core.Algorithms.PriorityQueue<PQElement>(new PQElementComparer());
            pq.Add(new PQElement(start, 0 + Heuristic(start, goal)));

            while (pq.Count > 0)
            {
                PQElement current = pq.Peek();
                pq.Remove();

                float f = current.f;
                Coords currentCoords = current.coords.Copy();
                List<Coords> currentPath = current.path;

                if (currentCoords == goal)
                    return current;

                if (visitedCoordsSet.Contains(currentCoords))
                    continue;

                if (visitedCoordsSet.Count > maximumSteps)
                {
                    return null;
                }

                visitedCoordsSet.Add(currentCoords);

                foreach (Coords next in ExpandNeighborNodes(currentCoords))
                {
                    float heu = Heuristic(next, goal);

                    List<Coords> new_path = currentPath.Select(coord => coord.Copy()).ToList();
                    new_path.Add(next);

                    pq.Add(new PQElement(f+1, next, new_path,f + 1 + heu));
                }
            }

            return null;
        }

        /// <summary>
        /// Returns all neighboring nodes, that are accessible from the position.
        /// </summary>
        private List<Coords> ExpandNeighborNodes(Coords coords)
        {
            Dictionary<int,Coords> neighboringPositions = tileMap.GetNeighboringPositions(coords);
            
            if (!tileManager.loadedTiles.ContainsKey(coords))
                return nonGeneratedIsTraversable ? neighboringPositions.Values.ToList() : new List<Coords>();

            TileInstance currentTile = tileManager.loadedTiles[coords];
            
            List<Coords> result = new();
            foreach ((int direction, Coords neighborCoords) in neighboringPositions)
            {
                if (!tileManager.loadedTiles.ContainsKey(neighborCoords))
                {
                    if (nonGeneratedIsTraversable)
                        result.Add(neighborCoords);
                    
                    continue;
                }

                TileInstance neighborTile = tileManager.loadedTiles[neighborCoords];

                if (CanMoveBetweenTiles(currentTile, neighborTile, direction))
                    result.Add(neighborCoords);
            }

            return result;
        }

        /// <summary>
        /// Returns if traversal can be made from tile currentTile to tile NeighborTile.
        /// This method should be overriden to define different traversal behavior or constraints.
        /// </summary>
        /// <param name="currentTile">Any instance of any tile.</param>
        /// <param name="neighborTile">A neighboring tile.</param>
        /// <param name="direction">Direction from current to neighbor</param>
        /// <returns>True if yes, false if no</returns>
        protected virtual bool CanMoveBetweenTiles(TileInstance currentTile, TileInstance neighborTile, int direction)
        {
            bool canExitInDirection = currentTile.mainTileData.GetBorder(direction).borderData.canExit;
            bool canEnterFromDirection = neighborTile.mainTileData.GetBorder(tileMap.OppositeSide(direction)).borderData.canEnter;

            int heightDifference = neighborTile.mainTileData.currentElevation - currentTile.mainTileData.currentElevation;
            bool canAscendHeight = heightDifference <= maxHeightUp;
            bool canDescendHeight = heightDifference >= maxHeightDown;
            
            return canExitInDirection && canEnterFromDirection && canAscendHeight && canDescendHeight;
        }

        /// <summary>
        /// A helper class for the priority queue.
        /// </summary>
        protected class PQElement
        {
            /// <summary>
            /// f is basically the current length of the path
            /// </summary>
            public readonly float f;
        
            /// <summary>
            /// Coords are the last addition to the path.
            /// </summary>
            public readonly Coords coords;
        
            /// <summary>
            /// Path is the candidate path for the search.
            /// </summary>
            public readonly List<Coords> path;

            /// <summary>
            /// A sum of this element's heuristic and non-heuristic priority
            /// </summary>
            public readonly float priority;

            public PQElement(Coords startCoords, float priority)
            {
                f = 0;
                coords = startCoords;
                path = new List<Coords>();

                this.priority = priority;
            }

            public PQElement(float f, Coords coords, List<Coords> path, float priority)
            {
                this.f = f;
                this.coords = coords;
                this.path = path;
                
                this.priority = priority;
            }
        }
        
        protected class PQElementComparer : IComparer<PQElement>
        {
            public int Compare(PQElement x, PQElement y)
            {
                return x!.priority.CompareTo(y!.priority);
            }
        }
    }
    
    
    
}
