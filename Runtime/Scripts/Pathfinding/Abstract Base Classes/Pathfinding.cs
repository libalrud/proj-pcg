using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace proj_pcg
{
    /// <summary>
    /// Base class for pathfinding.
    /// </summary>
    public abstract class Pathfinding : TileSystemBase
    {
        /// <summary>
        /// Async version of the GetPath method.
        /// </summary>
        public async Task<List<Coords>> GetPathAsync(Coords start, Coords end)
        {
            return await Task.Run(() => GetPath(start, end));
        }

        /// <summary>
        /// Calculates a shortest path between two coordinate positions.
        /// </summary>
        /// <param name="start">The starting coordinate</param>
        /// <param name="end">The ending coordinate</param>
        /// <returns>A list of neighboring coordinates from the start to the finish,
        /// including edge coordinates, or an empty list if a path wasn't found for any reason.</returns>
        public abstract List<Coords> GetPath(Coords start, Coords end);

    }
}
