using System;
using System.Collections.Generic;
using UnityEngine;

namespace proj_pcg
{
    /// <summary>
    /// An extension to A* pathfinding which shows the path in question
    /// </summary>
    public class VisualPathfinder : AStarPathfinding
    {
        /// <summary>
        /// What marker to use for path coordinates
        /// </summary>
        [SerializeField] [Header("Path visualisation")]
        [Tooltip("What marker to use for path coordinates")]
        private GameObject markerPrefab;

        /// <summary>
        /// How big should the markers be.
        /// </summary>
        [SerializeField] [Tooltip("How big should the markers be.")]
        private float markerScale = 1;

        /// <summary>
        /// How high above the terrain should markers be shown.
        /// </summary>
        [SerializeField] [Tooltip("How high above the terrain should markers be shown.")]
        private float markerElevation = 5;
        
        [Tooltip("The start position of pathfinding")]
        public Coords from;
    
        [Tooltip("The goal position of pathfinding")]
        public Coords to;
        
        [Tooltip("Length of a shortest path. Nan if none found")]
        public float pathLength;
    
        [Tooltip("List of coordinates of the found shortest path, if short enough.")]
        public List<Coords> path_visual;

        [NonSerialized]
        private List<Coords> path;
        
        private Coords lastFrom;
        private Coords lastTo;
            
        private List<GameObject> visualPath;
        private GameObject fromMarker;
        private GameObject toMarker;

        private void Awake()
        {
            fromMarker = Instantiate(markerPrefab);
            fromMarker.transform.localScale *= markerScale;
            toMarker = Instantiate(markerPrefab);
            toMarker.transform.localScale *= markerScale;

            visualPath = new List<GameObject>();
        }
        
        private void Update()
        {
            if (from != lastFrom || to != lastTo)
            {
                lastFrom = from.Copy();
                lastTo = to.Copy();
                RecalculateRoute();
                OnPathChanged();
            }
        }
        
        /// <summary>
        /// Triggered upon change of input parameters. Calculates a new shortest route.
        /// </summary>
        public void RecalculateRoute()
        {
            // A* from from to to
            // calculating = true;
            var result = AStar(from, to);

            if (result == null)
            {
                path = new List<Coords>();
                pathLength = float.NaN;
            }
            else
            {
                path = result.path;
                pathLength = result.f;

                path_visual = pathLength <= 100 
                    ? path 
                    : new List<Coords>(){new Coords(int.MinValue,int.MinValue)};
            }
        }
        
        private void OnPathChanged()
        {
            fromMarker.transform.position = tileMap.GetLocalPositionOfCoords(from) + markerElevation * Vector3.up;
            toMarker.transform.position = tileMap.GetLocalPositionOfCoords(to) + markerElevation * Vector3.up;

            foreach (GameObject go in visualPath)
            {
                Destroy(go);
            }

            visualPath = new List<GameObject>();

            if (path.Count > 100)
            {
                return;
            }

            foreach (var coords in path)
            {
                GameObject marker = Instantiate(markerPrefab);
                marker.transform.position = tileMap.GetLocalPositionOfCoords(coords) + markerElevation * Vector3.up;
                marker.transform.localScale *= markerScale / 2;
            
                visualPath.Add(marker);
            }
        }
    }
}
