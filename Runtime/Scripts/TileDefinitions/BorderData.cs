using System;
using System.Collections.Generic;
using UnityEngine;

namespace proj_pcg
{
    /// <summary>
    /// Two tuple data structure for Unity serialization.
    /// Effectively results in the same behavior as a Dictionary
    /// </summary>
    [Serializable]
    public class BorderDataPair
    {
        /// <summary>
        /// A reference to another BorderData instance, or this own one.
        /// </summary>
        public BorderData border;
        
        /// <summary>
        /// The weight represents the amount of compatibility between two Borderdata instances.
        /// 0 means "don't use unless necessary" (soft zero).
        /// Remove the value completely to achieve a hard zero.
        /// </summary>
        [Range(0, 1)] [Tooltip("The weight represents the amount of compatibility between two BorderData " +
                                     "instances. 0 means \"don't use unless necessary\" (soft zero). Remove the value " +
                                     "completely to achieve a hard zero.")]
        public float weight;

        public BorderDataPair(BorderData border, float weight)
        {
            this.border = border;
            this.weight = weight;
        }
    }

    /// <summary>
    /// Contains all the parameters that are the same for each instance of a specific border.
    /// </summary>
    [CreateAssetMenu(menuName = "BorderData", order = 4, fileName = "NewBorderData")]
    [Serializable]
    public class BorderData: ScriptableObject
    {
        
        /// <summary>
        /// An unique border name. Must be unique!
        /// </summary>
        [Tooltip("An unique border name. Must be unique!")]
        public string borderName;

        /// <summary>
        /// A list of compatible borders. Represents a graph of border compatibilities.
        /// Should at all times be kept symmetrical (if border A is compatible with B, then B with A).
        /// </summary>
        [Tooltip("A list of compatible borders. Represents a graph of border compatibilities. Should at all " +
                 "times be kept symmetrical (if border A is compatible with B, then B with A).")]
        public List<BorderDataPair> borders;

        private List<BorderDataPair> lastBorders;

        /// <summary>
        /// can exit tile through this border
        /// </summary>
        [Tooltip("Tile can be entered or exited through this border.")]
        public bool canExit = true;
    
        /// <summary>
        /// can enter tile through this border
        /// </summary>
        [Tooltip("Tile can be entered or exited through this border.")]
        public bool canEnter = true;

        private void Reset()
        {
            borderName = "Default";

            borders = new List<BorderDataPair>(){new BorderDataPair(this, 1f)};
            lastBorders = new List<BorderDataPair>();
        }

        private void OnValidate()
        {
            // duplicate all compatibilities to other side
            // e.g. other was added to this; this should be added in other
            // e.g. make symmetrical
            RemoveCopyFromOther();
            AddCopyToOther();

            lastBorders.Clear();
            foreach (BorderDataPair borderDataPair in borders)
            {
                lastBorders.Add(borderDataPair);
            }
        }

        /// <summary>
        /// Finds any subtractive changes and copies them into other object.
        /// E.g. Removing border A from border B's list will also remove
        /// border B from A's list.
        /// </summary>
        private void RemoveCopyFromOther()
        {
            lastBorders ??= new List<BorderDataPair>();
            
            // subtractive change is anything that is in lastBorders but not in borders.
            // if Q was in R and R in Q, but now Q is not in R, it should find R in Q and remove it.
            // if Q was in this, but now Q is not in this, it should find this in Q and remove it.
            foreach (BorderDataPair lastBdp in lastBorders)
            {
                if (lastBdp.border == null)
                    continue;

                // is in lastBorders and in borders -> nothing changed
                // if (lastBdp.In(borders))
                //     continue;

                if (BDPHas(borders, lastBdp))
                {
                    continue;
                }

                // here if lastBdp was removed.
                foreach (BorderDataPair maybeRemovedBdp in lastBdp.border.borders)
                {
                    if (maybeRemovedBdp == null)
                    {
                        continue;
                    }
                    
                    if (maybeRemovedBdp.border.name == this.name)
                    {
                        lastBdp.border.borders.Remove(maybeRemovedBdp);
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Helper method to find if element is present
        /// </summary>
        private static bool BDPHas(List<BorderDataPair> borders, BorderDataPair lastBdp)
        {
            if (lastBdp.border == null)
            {
                return false;
            }
            
            foreach (BorderDataPair otherBdp in borders)
            {
                if (otherBdp.border == null)
                {
                    continue;
                }
                
                if (lastBdp.border.name == otherBdp.border.name)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Finds any additive changes and copies them into other object.
        /// E.g. Adding border A to border B's list will also add
        /// border B to A's list.
        /// Same goes for editing the weight.
        /// </summary>
        private void AddCopyToOther()
        {
            foreach (BorderDataPair bdp in borders)
            {
                if (bdp.border == null)
                    continue;

                foreach (BorderDataPair otherBdp in bdp.border.borders)
                {
                    if (otherBdp.border == null)
                    {
                        return;
                    }
                    
                    // if value present in other
                    if (name == otherBdp.border.name)
                    {
                        otherBdp.weight = bdp.weight;

                        return;
                    }
                }
                
                // if not present in other
                bdp.border.borders.Add(new BorderDataPair(this, bdp.weight));
            }
        }
    }
}
