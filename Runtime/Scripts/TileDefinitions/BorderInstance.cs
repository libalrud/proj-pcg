using System;
using UnityEngine;

namespace proj_pcg
{
    /// <summary>
    /// A borderData + a relative elevation difference.
    /// Used for horizontal borders.
    /// </summary>
    [Serializable]
    public class BorderInstance
    {
        public BorderData borderData;
        
        /// <summary>
        /// How much offset should a neighboring tile be.
        /// </summary>
        [Tooltip("How much offset should a neighboring tile be.")]
        public int relativeElevation;
    }

    /// <summary>
    /// Like BorderInstance, but for vertical borders,
    /// e.g. borders between layers of the same tile coordinate.
    /// </summary>
    [Serializable]
    public class VerticalBorderInstance
    {
        /// <summary>
        /// The border corresponding to the verical border of the tile towards its layers.
        /// </summary>
        [Tooltip("The border corresponding to the verical border of the tile towards its layers.")]
        public BorderData vertBorderData;

        /// <summary>
        /// The offset of a tile layer towards the main tile. Defaults to zero. Ignored for the main layer.
        /// </summary>
        [Tooltip("The offset of a tile layer towards the main tile. Defaults to zero. Ignored for the main layer.")]
        public Vector3 offset;
    }
    
}