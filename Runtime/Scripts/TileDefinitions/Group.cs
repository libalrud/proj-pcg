using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace proj_pcg
{
    
    /// <summary>
    /// Defines a group of tiles to be used by TileManager and HeightMap.
    /// A group is a bunch of tiles that are supposed to appear more commonly together.
    /// Tiles from different groups will rarely share a border.
    /// Like biomes; each biome has a different composition,
    /// a different size (think of river x lake x mountain),
    /// and a different terrain profile.
    /// </summary>
    // [CreateAssetMenu(menuName = "TileGroup", order = 1, fileName = "NewTileGroup")]
    [Serializable]
    public class Group
    {
        /// <summary>
        /// Automatically allocated id.
        /// </summary>
        [ReadOnly] [Tooltip("Automatically allocated id.")]
        public int id;

        /// <summary>
        /// 0 if on main layer, else number starting from 1.
        /// </summary>
        [ReadOnly] [Tooltip("0 if on main layer, else number starting from 1.")]
        public int layerID;

        /// <summary>
        /// Unique group name.
        /// </summary>
        [SerializeField] [Tooltip("Unique group name. Must be unique!")]
        public string groupName;

        /// <summary>
        /// Tiles that are part of this Group --- a reference to the layer
        /// </summary>
        [FormerlySerializedAs("mainLayerTiles")] 
        [Tooltip("Tiles that are part of this Group --- a reference to the layer")]
        public Layer tiles;
        
        /// <summary>
        /// Tiles but one for each rotation
        /// </summary>
        [NonSerialized]
        public Layer compiledTiles;

        /// <summary>
        /// For debug purposes.
        /// </summary>
        public Group(string name)
        {
            groupName = name;
        }

        /// <summary>
        /// Used for default group names.
        /// </summary>
        private static int groupCount = 0;

        public Group() : this($"Default name #{groupCount++}")
                        {
                            }

        #region Standard methods

        public static bool operator== (Group c1, Group c2)
        {
            if ((object)c1 == null)
                return (object)c2 == null;

            return c1.Equals(c2);
        }
    
        public static bool operator!= (Group c1, Group c2)
        {
            return !(c1 == c2);
        }
        
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Group) obj);
        }

        protected bool Equals(Group other)
        {
            if (ReferenceEquals(other, null))
                return false;

            return groupName == other.groupName;
        }

        public override int GetHashCode()
        {
            // ReSharper disable once NonReadonlyMemberInGetHashCode
            // not readonly for serialization purposes, but effectively never changes.
            return groupName.GetHashCode();
        }

        public override string ToString()
        {
            return $"Group {groupName} ({tiles.Count} tiles)";
        }

        #endregion
    }
}