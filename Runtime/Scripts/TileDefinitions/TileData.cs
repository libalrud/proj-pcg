using System;
using Newtonsoft.Json;
using UnityEngine;

namespace proj_pcg
{
    /// <summary>
    /// The most important data structure: a TileData.
    /// Captures the essence of a tile: its elevation, rotation, its physical form, its identity.
    /// Contains editor-based input fields for relavant data like weight etc.
    /// </summary>
    [CreateAssetMenu(menuName = "TileData", order = 2, fileName = "NewTileData")]
    [JsonObject(MemberSerialization.OptIn)]
    [Serializable]
    public class TileData : ScriptableObject
    {
        /// <summary>
        /// Arbitrary ID evaluated at runtime. For main layer tiles ids are unique and increment from zero. All objects of the same type have the same TileData.
        /// -2 is invalid value. -1 typically means no tile was compatible.
        /// </summary>
        [Tooltip("rbitrary ID evaluated at runtime. For main layer tiles ids are unique and increment from zero. " +
                 "All objects of the same type have the same TileData. -2 is invalid value. -1 typically means " +
                 "no tile was compatible.")]
        [JsonProperty] [ReadOnly]
        public int id = -2;
        
        /// <summary>
        /// user-defined data: the physical form.
        /// </summary>
        [Tooltip("GameObject representing the tile.")]
        public GameObject prefab;

        /// <summary>
        /// How often the tile type appears.
        /// </summary>
        [Range(0,1)] [Tooltip("How often the tile type appears.")]
        public float weight = 1;

        /// <summary>
        /// Must be less than totalTileSides from TileMap data
        /// </summary>
        [Tooltip("Represents how many rotations are unique.")]
        public int uniqueRotations = 4;
        
        /// <summary>
        /// Sets the lower bound of the elevation range of the tile.
        /// Algorithmic complexity is >= O( (max-min)**2 ).
        /// For example, tile representing the sea can have a range of 0-0, and snowy mountains 5-10.
        /// </summary>
        [Tooltip("Sets the lower/upper bound of the elevation range of the tile.")]
        public int minElevation;
        
        /// <summary>
        /// Sets the upper bound of the elevation range of the tile.
        /// Algorithmic complexity is O( (max-min)**2 ).
        /// For example, tile representing the sea can have a range of 0-0, and snowy mountains 5-10.
        /// </summary>
        [Tooltip("Sets the lower/upper bound of the elevation range of the tile.")]
        public int maxElevation;
        
        /// <summary>
        /// Two TileData with different elevations are not the same.
        /// Auto generated upon startup
        /// </summary>
        [JsonProperty] [ReadOnly] [Tooltip("Two TileData with different elevations are not the same.")]
        public int currentElevation = 0;

        /// <summary>
        /// Define border types on all sides.
        /// Start at -z and move on clockwise.
        /// (For rectangles: -z = north, -x = east, +z, +x)
        /// </summary>
        [Tooltip("Define border types on all sides. Start at -z and move on clockwise. " +
                       "(For rectangles: -z = north, -x = east, +z, +x)")]
        public BorderInstance[] sideBorders = new BorderInstance[4];

        /// <summary>
        /// Specifies the border compatibility for the top and bottom sides of the tile.
        /// </summary>
        [Tooltip("Specifies the border compatibility for the top and bottom sides of the tile.")]
        public VerticalBorderInstance verticalBorder = new VerticalBorderInstance();


        // auto filled data
        
        /// <summary>
        /// The tile's group it belongs to.
        /// </summary>
        [HideInInspector]
        public Group group;
        
        /// <summary>
        /// Two TileData with different rotations are not the same.
        /// Auto generated upon startup
        /// </summary>
        [JsonProperty] [HideInInspector] 
        public int currentRotation = 0;

        public int totalRotations => sideBorders.Length;
        public float currentRotationDegrees => 360f * currentRotation / totalRotations;

        private void OnValidate()
        {
            uniqueRotations.Clamp(0, totalRotations);

            if (minElevation > maxElevation)
            {
                minElevation = maxElevation = (minElevation + maxElevation) / 2;
            }
        }

        /// <summary>
        /// Get border like in .borders[direction] but supporting wrapping the integer value
        /// </summary>
        public BorderInstance GetBorder(int direction) => sideBorders[(direction + sideBorders.Length) % sideBorders.Length];

        /// <summary>
        /// Creates a tile instance as a tile copy with given parameters
        /// </summary>
        /// <param name="newID">The ID of the tile. 0-indexed, incremental.
        /// Each layer must have its own indexing starting from zero.</param>
        /// <param name="rotation">The rotation of the tile.</param>
        /// <param name="elevation">The elevation of the tile, between minElevation and maxElevation</param>
        /// <param name="tileGroup">The group the tile belongs to</param>
        /// <returns>A new TileData instance</returns>
        public TileData InstantiateAndRotate(int newID, int rotation, int elevation, Group tileGroup)
        {
            TileData newTileData = Instantiate(this);
            newTileData.id = newID;
            newTileData.group = tileGroup;

            newTileData.currentElevation = elevation;
            newTileData.currentRotation = rotation;

            newTileData.sideBorders.RotateArray(amount: rotation);

            return newTileData;
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }

        public override bool Equals(object other)
        {
            if (other == null || GetType() != other.GetType())
                return false;

            TileData otherTile = (TileData) other;
            return this.id == otherTile.id && this.group == otherTile.group;
        }
        
        public static bool operator== (TileData t1, TileData t2)
        {
            if ((object)t1 == null)
                return (object)t2 == null;
    
            return t1.Equals(t2);
        }
    
        public static bool operator!= (TileData t1, TileData t2)
        {
            return !(t1 == t2);
        }

        public override string ToString()
        {
            return $"#{id}: {name.Replace("(Clone)", "")}, " +
                   $"rotation {currentRotation}/{totalRotations}, " +
                   $"elev. {currentElevation}";
        }
    }

}
