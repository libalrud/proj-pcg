using System;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine.Serialization;

namespace proj_pcg
{
    /// <summary>
    /// Instance of a tile w/ all parameters
    /// "A tile at some coordinates. What is it."
    /// Does not have a reference to its instance though.
    /// </summary>
    [Serializable]
    [JsonObject(MemberSerialization.OptIn)]
    public class TileInstance
    {
        /// <summary>
        /// Corresponds to mainTileData.id.
        /// Used for JSON serialization.
        /// </summary>
        [JsonProperty("id")] 
        public int tileDataId;
        
        [JsonProperty("pos")]
        public Coords coords;

        /// <summary>
        /// How much above center in absolute units
        /// </summary>
        [JsonProperty("elev")] 
        public int elevation;

        /// <summary>
        /// Automatically assigned a reference at startup
        /// </summary>
        public TileData mainTileData;

        /// <summary>
        /// Layer's tile data
        /// </summary>
        [FormerlySerializedAs("layer")] [FormerlySerializedAs("otherTileData")] 
        public TileData[] layerTileData;

        /// <summary>
        /// Same ID's as in layerTileData[n].id
        /// Used for JSON serialization.
        /// </summary>
        [JsonProperty("layer")]
        public int[] layerTileIds;

        /// <summary>
        /// True if the GameObject is represented in the world (is loaded, is close to player, ...)
        /// </summary>
        public bool isActive;

        /// <summary>
        /// For use by the JSON serializer
        /// Only adds position and tileDataId
        /// </summary>
        public TileInstance(TileData mainTileData, TileData[] layerTileData, Coords coords, int elevation)
        {
            this.mainTileData = mainTileData;
            tileDataId = mainTileData.id; 
            
            this.layerTileData = layerTileData;
            layerTileIds = layerTileData.Select(tileData => tileData == null? -1 : tileData.id).ToArray();
        
            this.coords = coords; 
            this.elevation = elevation;
        
            isActive = false;
        }

        /// <summary>
        /// Used only for JSON serialization
        /// </summary>
        public TileInstance() { }

        public override int GetHashCode()
        {
            var hash = 29;
            hash = hash * 37 + coords.GetHashCode();
            hash = hash * 37 + tileDataId.GetHashCode();
            return hash;
        }
        
        public override string ToString()
        {
            return $"TDI={tileDataId}, active={isActive}";
        }

        protected bool Equals(TileInstance other)
        {
            if (layerTileData.Length != other.layerTileData.Length)
            {
                return false;
            }
            
            for (int i = 0; i < layerTileData.Length; i++)
            {
                if (layerTileData[i] != other.layerTileData[i])
                    return false;
            }
            
            return coords == other.coords && 
                   elevation == other.elevation &&
                   mainTileData == other.mainTileData && 
                   isActive == other.isActive;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TileInstance) obj);
        }
    }
}
