using System.Collections.Generic;
using UnityEngine;

namespace proj_pcg
{
    public class OptimalOrderTileManager : TileManager
    {
        private int lastLoadedN = 0;

        // function spiral(n)
        // k=ceil((sqrt(n)-1)/2)
        // t=2*k+1
        // m=t^2 
        // t=t-1
        // if n>=m-t then return k-(m-n),-k        else m=m-t end
        // if n>=m-t then return -k,-k+(m-n)       else m=m-t end
        // if n>=m-t then return -k+(m-n),k else return k,k-(m-n-t) end
        //     end
        /// <summary>
        /// https://math.stackexchange.com/questions/163080/
        /// </summary>
        private Coords GetNthCoords(int n)
        {
            int k = Mathf.CeilToInt((Mathf.Sqrt(n) - 1) / 2);
            int t = 2 * k + 1;
            int m = t * t;
            t--;

            if (n >= m-t)
                return new Coords(k - (m - n), -k);
            
            m -= t;

            if (n >= m-t)
                return new Coords(-k,-k + (m - n));
            
            m -= t;

            if (n >= m-t)
                return new Coords(-k + (m - n),k);
            
            return new Coords(k,k - (m - n - t));
        }
        
        protected override void ProcessTilesToLoad(Coords coords)
        {
            while (!loadedTiles.ContainsKey(coords))
            {
                Coords nextCoords = GetNthCoords(lastLoadedN ++);
                GenerateTile(nextCoords);
            }
                
            tileMap.EnqueueRequest(loadedTiles[coords], true);
        }
    }
}
