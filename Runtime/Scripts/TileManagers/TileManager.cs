using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

namespace proj_pcg
{
    /// <summary>
    /// TileManager does the "organisation" part of the generation.
    /// Enforces correct synchronisation, calls data requests to all other components, etc.
    /// Pools objects.
    /// </summary>
    public class TileManager : TileSystemBase
    {
        /// <summary>
        /// Use this to identify if you have multiple generators in your project.
        /// Non alphanumerics are removed from name. Must not be empty.
        /// </summary>
        [SerializeField] [Tooltip("Use this to identify if you have multiple generators in your project. " +
                                        "Non alphanumerics are removed from name. Must not be empty.")]
        public string tileMapName = "default";
        
        /// <summary>
        /// How near to the reference point (typically the player)
        /// are tiles loaded and/or activated.
        /// </summary>
        [Header("Tile loading parameters")]
        [SerializeField] [Tooltip("How near to the reference point (typically the player) " +
                                  "are tiles loaded and/or activated.")]
        protected int loadDistance = 10;
        
         /// <summary>
         /// How many tiles to generate each frame with Oracle, maximum.
         /// </summary>
         [SerializeField] [Tooltip("How near to the reference point (typically the player) " +
                                         "are tiles loaded and/or activated.")]
         protected int generateTilesPerFrame = 5;
         
        /// <summary>
        /// The input TileData lists. Will be automatically rotated upon start.
        /// At least one tile has to be specified for each Group. Groups must have unique names.
        /// </summary>
        [Header("Input Values")] [SerializeField] 
        [Tooltip("The input TileData lists. Will be automatically rotated upon start. At least one tile " +
                       "has to be specified for each Group. Groups must have unique names.")]
        public Group[] mainLayerGroups;
        
        
        /// <summary>
        /// Any other additional layers. Mainly for decoration purposes. Doesn't have a structure of its own.
        /// </summary>
        [SerializeField] [Tooltip("Any other additional layers. Mainly for decoration purposes. " +
                                  "Doesn't have a structure of its own.")]
        public Group[] additionalLayers;

        /// <summary>
        /// Pre-compiled tiles are added to this list. Do NOT edit it manually.
        /// Updates in edit-time.
        /// </summary>
        [Header("Runtime Values / shown for debug purposes")]
        [Tooltip("Pre-compiled tiles are added to this list. Do NOT edit it manually. Updates in edit-time.")] [ReadOnly]
        public Layer mainLayerTileList;

        /// <summary>
        /// Pre-compiled tiles are added to this list. Do NOT edit it manually.
        /// Updates in edit-time.
        /// </summary>
        [Tooltip("Pre-compiled tiles are added to this list. Do NOT edit it manually. Updates in edit-time.")] [ReadOnly]
        public List<Layer> additionalLayerList;
        
        /// <summary>
        /// Access to loaded tiles by coordinate.
        /// Not serializable.
        /// </summary> 
        public Dictionary<Coords, TileInstance> loadedTiles;
        
        /// <summary>
        /// Which tiles were loaded as of last frame.
        /// </summary>
        private HashSet<Coords> lastLoadedTiles;
        
        /// <summary>
        /// A queue of load/unload requests.
        /// </summary>
        private Queue<Request> loadQueue;

        private void Reset()
        {
            mainLayerGroups = Array.Empty<Group>();
            additionalLayers = Array.Empty<Group>();
        }

        private void OnValidate()
        {
            Awake();
            
            if (terrainMap != null) 
                terrainMap.OnValidate();
        }
        
        private void Awake()
        {
            loadedTiles = new Dictionary<Coords, TileInstance>();
            lastLoadedTiles = new HashSet<Coords>();
            loadQueue = new Queue<Request>();

            CheckInputVariables();

            LoadTileTypes();
        }
        
        private void Start()
        {
            oracle.CalculateCompatibilities(mainLayerTileList, additionalLayerList);

            LoadSavedState();
        }
        
        private void Update()
        {
            EnqueueRequests();
            ProcessRequests();
        }

        /// <summary>
        /// Checks input consistency.
        /// </summary>
        private void CheckInputVariables()
        {
            if (Regex.Replace(tileMapName, "[^A-Za-z0-9]", "") != tileMapName || tileMapName == "")
                Debug.LogError("Invalid tile map name! Must consist of a-Z, 0-9. Must not be empty. This WILL break the system.");

            if (loadDistance < 0)
                loadDistance = 0;

            if (generateTilesPerFrame < 0)
                generateTilesPerFrame = 0;

            for (int i = 0; i < additionalLayers.Length; i++)
            {
                if (!additionalLayers[i].groupName.Contains($"Layer {i}"))
                {
                    additionalLayers[i].groupName = $"Layer {i}";
                }
            }
        }
         
        /// <summary>
        /// Initializes TileData id's and instantiates rotations and height variants
        /// </summary>
        private void LoadTileTypes()
        {
            int groupCount = 0;
            
            mainLayerTileList = new Layer();

            foreach (Group mainLayerGroup in mainLayerGroups)
            {
                mainLayerGroup.id = groupCount++;
                mainLayerGroup.layerID = 0;
                
                Layer tileVariations = GenerateTileVariations(mainLayerGroup, mainLayerTileList.tiles.Count);

                mainLayerGroup.compiledTiles = tileVariations;
                
                mainLayerTileList += tileVariations;
            }

            additionalLayerList = new List<Layer>();

            int i = 1;
            foreach (Group layerGroup in additionalLayers)
            {
                layerGroup.id = groupCount++;
                layerGroup.layerID = i++;
                
                additionalLayerList.Add(GenerateTileVariations(layerGroup));
            }
        }

        /// <summary>
        /// Converts input TileData structures into internal versions.
        /// These are visible in the UI but not modifiable.
        /// </summary>
        private Layer GenerateTileVariations(Group inputGroup, int startingID = 0)
        {
            Layer layer = new Layer();

            foreach (var tile in inputGroup.tiles)
            {
                if (tile == null)
                    continue;

                for (int rotation = 0; rotation < tile.uniqueRotations; rotation++)
                {
                    for (int elevation = tile.minElevation; elevation <= tile.maxElevation; elevation++)
                    {
                        layer.Add(tile.InstantiateAndRotate(startingID++, rotation, elevation, inputGroup));   
                    }
                }
            }

            return layer;
        }

        /// <summary>
        /// Processes loaded state from storage.
        /// </summary>
        private void LoadSavedState()
        {
            // loaded state overwrites initial state
            
            Dictionary<Coords, TileInstance> tilesFromLoader = tileSaver.LoadAllTiles();
            
            foreach (var (coords, tileInstance) in tilesFromLoader)
            {
                loadedTiles[coords] = tileInstance;
                
                oracle.OnTileGenerated(tileInstance, mainLayerTileList);
            }
        }

        /// <summary>
        /// Overridable method defining which tile coords are "near" some coord.
        /// By default, the selection represents a square with a diameter of 2 * range + 1.
        /// </summary>
        /// <param name="nearestTile">The tile in the center</param>
        /// <param name="range">How far away can tiles be to still be selected.</param>
        /// <returns>A HashSet of all coords, which are considered
        /// to be nearer to the center tile than the given range.</returns>
        public virtual HashSet<Coords> GetCoordsWithinRange(Coords nearestTile, int range)
        {
            HashSet<Coords> nearTiles = new();

            if (range < 0)
                return nearTiles;

            // rectangle:
            for (int x = -range; x <= range; x++)
            {
                for (int z = -range; z <= range; z++)
                {
                    nearTiles.Add(nearestTile + new Coords(x, z));
                }
            }

            return nearTiles;
        }

        public Dictionary<int, TileData> GetNeighboringTilesLayers(Coords coords, int layerID = 0)
        {
            Dictionary<int, TileData> results = new();
            
            foreach ((int key, Coords value) in tileMap.GetNeighboringPositions(coords))
            {
                if (!value.In(loadedTiles.Keys))
                    continue;

                TileInstance currentTile = loadedTiles[value];

                if (layerID == 0)
                {
                    results[key] = currentTile.mainTileData;
                    continue;
                }

                if (layerID - 1 >= currentTile.layerTileData.Length)
                {
                    throw new IndexOutOfRangeException(
                        $"Trying to retrieve data for layer {layerID}, " +
                        $"but there seem to be only {currentTile.layerTileData.Length} layers");
                }
                
                TileData neighbor = loadedTiles[value].layerTileData[layerID - 1];

                if (neighbor == null)
                    continue;
                
                results[key] = neighbor;
            }

            return results;
        }

        /// <summary>
        /// Calculate which tiles are to be unloaded and loaded based on difference in sets of near tiles
        /// </summary>
        private void EnqueueRequests()
        {
            HashSet<Coords> nearTiles = GetCoordsWithinRange(
                tileMap.GetNearestTileToReferencePoint(), loadDistance);

            IEnumerable<Coords> tilesToLoad = new HashSet<Coords>(nearTiles).Except(lastLoadedTiles);
            IEnumerable<Coords> tilesToUnload = new HashSet<Coords>(lastLoadedTiles).Except(nearTiles);

            lastLoadedTiles = nearTiles;

            foreach (Coords coords in tilesToLoad)
            {
                loadQueue.Enqueue(new Request(coords, true));
            }

            foreach (Coords coords in tilesToUnload)
            {
                loadQueue.Enqueue(new Request(coords, false));
            }
        }
        
        /// <summary>
        /// Processes up to the allowed number of requests each frame
        /// </summary>
        private void ProcessRequests()
        {
            for (int i = 0; i < generateTilesPerFrame && loadQueue.Count > 0;)
            {
                Request nextRequest = loadQueue.Dequeue();

                if (nextRequest.load)
                {
                    ProcessTilesToLoad(nextRequest.coords);
                    i++;
                }
                else
                {
                    ProcessTilesToUnload(nextRequest.coords);
                }
            }
        }

        /// <summary>
        /// Sends a deactivation request to the TileMap
        /// </summary>
        /// <param name="coords"></param>
        protected virtual void ProcessTilesToUnload(Coords coords)
        {
            tileMap.EnqueueRequest(loadedTiles[coords], false);
        }

        /// <summary>
        /// Generates a tile and then sends an activation request to the TileMap
        /// Can be overrided to any other behavior
        /// </summary>
        /// <param name="coords"></param>
        protected virtual void ProcessTilesToLoad(Coords coords)
        {
            if (!loadedTiles.ContainsKey(coords))
            {
                GenerateTile(coords);
            }

            tileMap.EnqueueRequest(loadedTiles[coords], true);
        }

        /// <summary>
        /// Generates a tile and updates statistics.
        /// </summary>
        /// <param name="coords"></param>
        protected void GenerateTile(Coords coords)
        {
            TileInstance newTile = oracle.GenerateTileAtCoords(mainLayerTileList, additionalLayerList, coords);
            
            tileStats.OnTileGenerated(newTile, newTile.tileDataId == -1);
            loadedTiles[coords] = newTile;
        }

        
        #if UNITY_EDITOR
        
        /// <summary>
        /// Projects a ray from the mouse onto the plane of the tile map and calculates the nearest tile coordinates.
        /// Doesn't account for tile elevation.
        /// Editor only!
        /// </summary>
        /// <param name="clickPosition">Screen position of the click</param>
        /// <returns>The coordinates nearest to the tile click.</returns>
        /// <exception cref="Exception">If the click doesn't project onto the tile map.</exception>
        public Coords GetTileNearestToClick(Vector3 clickPosition)
        {
       
            Ray ray = UnityEditor.SceneView.currentDrawingSceneView.camera.ScreenPointToRay(clickPosition);
            Plane plane = new Plane(tileManager.transform.up, 0);
            
            Vector3 lastActivePosition;
            if (plane.Raycast(ray, out float distance))
                lastActivePosition = ray.GetPoint(distance);
            else
                throw new Exception("Invalid raycast");

            return tileMap.GetNearestTile(lastActivePosition);
            
        }
        #endif
        
        /// <summary>
        /// Simple struct to help with tile generation+unload requests
        /// </summary>
        private struct Request
        {
            public Coords coords;
            public bool load;

            public Request(Coords coords, bool load)
            {
                this.coords = coords;
                this.load = load;
            }
        }
    }

}
