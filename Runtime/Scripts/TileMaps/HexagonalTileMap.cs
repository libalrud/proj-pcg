using System;
using System.Collections.Generic;
using UnityEngine;

namespace proj_pcg
{
    /// <summary>
    /// Implements a hexagonal tile ordering.
    /// Axial coordinate system as described by Isaac:
    /// https://stackoverflow.com/questions/2459402/hexagonal-grid-coordinates-to-pixel-coordinates/2459541#2459541
    /// Default hexagonal mesh:
    /// "Hexagon (no textures)" (https://skfb.ly/6TzWM) by iamsosha is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).
    /// </summary>
    public class HexagonalTileMap : TileMap
    {
        /// <summary>
        /// How wide is a hexagon. Must be positive.
        /// </summary>
        [SerializeField] private float tileSize = 1;

        /// <summary>
        /// Saves like 4 chars in all 2 use cases
        /// </summary>
        private const float sqrt3 = 1.73205080757f;
        
        // y = 3/2 * s * b
        // b = 2/3 * y / s
        // x = sqrt(3) * s * ( b/2 + r)
        // x = - sqrt(3) * s * ( b/2 + g )
        // r = (sqrt(3)/3 * x - y/3 ) / s
        // g = -(sqrt(3)/3 * x + y/3 ) / s
        //
        // r + b + g = 0

        protected override Vector3 GetPositionOfCoords(Coords coords)
        {
            float x = sqrt3 * tileSize * (coords.z / 2f + coords.x);
            float y = 3f / 2f * tileSize * coords.z;
            return new Vector3(x, 0, y);
        }

        protected override float GetLocalTileRotation(TileInstance tileInstance)
        {
            return 0; 
        }

        protected override Coords GetNearestTileCoords(Vector3 localPosition)
        {
            // project to tile map
            float x = localPosition.x;
            float y = localPosition.z;

            float r = (sqrt3 / 3f * x - y / 3f) / tileSize;
            float b = 2f / 3f * y / tileSize;

            return new Coords(Mathf.RoundToInt(r), Mathf.RoundToInt(b));
        }

        public override Dictionary<int, Coords> GetNeighboringPositions(Coords coords)
        {
            int r = coords.x; // red is down-right
            int b = coords.z; // blue is up

            var result = new Dictionary<int, Coords>()
            {
                {0, new Coords(r, b+1)},
                {1, new Coords(r+1, b)},
                {2, new Coords(r+1, b-1)},
                {3, new Coords(r, b-1)},
                {4, new Coords(r-1, b)},
                {5, new Coords(r-1, b+1)}
            };

            return result;
        }

        public override int OppositeSide(int side)
        {
            // 1->4
            // 2->5
            return (side + 6 / 2) % 6;
        }
    }
}
