using System.Collections.Generic;
using UnityEngine;

namespace proj_pcg
{
    
    /// <summary>
    /// Implements a rectangular ordering. Tiles can be square or oblong.
    /// </summary>
    public class RectangleTileMap : TileMap
    {
        /// <summary>
        /// How big is one tile. Can be square or oblong.
        /// Must have positive sizes.
        /// Coordinate calculation can be overriden for different tile sizes.
        /// </summary>
        [SerializeField] [Tooltip("How big is one tile. Can be square or oblong. Must have positive sizes. " +
                                        "Coordinate calculation can be overriden for different tile sizes.")]
        private Vector2 tileSize = new Vector2(1, 1);
        
        protected override Vector3 GetPositionOfCoords(Coords coords)
        {
            Vector2 pos = coords * tileSize;
            return new Vector3(pos.x, 0, pos.y);
        }

        protected override float GetLocalTileRotation(TileInstance tileInstance)
        {
            return 0; 
        }

        protected override Coords GetNearestTileCoords(Vector3 localPosition)
        {
            // project to tile map
            int posX = (int) (localPosition.x / tileSize.x + 0.5f * Mathf.Sign(localPosition.x));
            int posZ = (int) (localPosition.z / tileSize.y + 0.5f * Mathf.Sign(localPosition.z));

            Coords result = new Coords(posX, posZ);

            return result;
        }

        public override Dictionary<int, Coords> GetNeighboringPositions(Coords coords)
        {
            int x = coords.x;
            int z = coords.z;
            
            return new Dictionary<int, Coords>()
            {
                {0, new Coords(x,   z-1)},
                {1, new Coords(x-1, z  )},
                {2, new Coords(x,   z+1)},
                {3, new Coords(x+1, z  )}
            };
        }

        public override int OppositeSide(int side)
        {
            // 1->3
            // 2->0
            return (side + 4 / 2) % 4;
        }
    }
}
