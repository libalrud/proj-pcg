using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace proj_pcg
{
 
    // Communicates with Unity and TileManager
    // - manages in-world tile representations ( loading and unloading tiles )
    // - knows position of all loaded tiles
    // - knows which tiles are to be loaded/unloaded. 
    public abstract class TileMap : TileSystemBase
    {
        /// <summary>
        /// Reference point marks the point around which
        /// all tiles will be loaded/unloaded
        /// </summary>
        [SerializeField] [Tooltip("Reference point marks the point around which all tiles will be loaded/unloaded")]
        public Transform referencePoint;
        
        /// <summary>
        /// Limit of activated (instantiated) and deactivated (destroyed) tiles per frame.
        /// Must be > 0. Recommended less than 20.
        /// </summary>
        [SerializeField] [Range(1, 100)] [Tooltip("Limit of activated (instantiated) and deactivated (destroyed) " +
                                                        "tiles per frame. Must be > 0. Recommended less than 20.")]
        protected int activateTilesPerFrame = 5;

        /// <summary>
        /// How tall is a difference of 1 in elevation.
        /// A tile that has a left border of -1 and a right border of +1 will change elevation by twice this coefficient.
        /// </summary>
        [SerializeField] [Tooltip("How tall is a difference of 1 in elevation.\n A tile that has a left border of -1 " +
                                  "and a right border of +1 will change elevation by twice this coefficient.")] 
        public float elevationOffsetCoefficient = 1f;

        /// <summary>
        /// How many rotations do tiles have. Undefined behavior if there are multiple different-sided tiles.
        /// </summary>
        public int totalTileRotations => tileManager.mainLayerTileList[0].totalRotations;

        protected LinkedListQueue<TileInstance> tilesToActivateQueue;
        protected LinkedListQueue<TileInstance> tilesToDeactivateQueue;
        
        protected Dictionary<Coords, GameObject> activeObjects;
        protected Transform tileMapTransform;

        /// <summary>
        /// Adds a tile load or unload request to be processed
        /// load = activate and instantiate
        /// unload = deactivate and destroy
        /// </summary>
        /// <param name="tile">The tile to be processed</param>
        /// <param name="load">True - load tile, False - unload</param>
        public void EnqueueRequest(TileInstance tile, bool load)
        {
            // if in both queues, remove from both
            if (load && tilesToDeactivateQueue.Contains(tile))
            {
                tilesToDeactivateQueue.Remove(tile);
                return;
            }

            if (!load && tilesToActivateQueue.Contains(tile))
            {
                tilesToActivateQueue.Remove(tile);
                return;
            }


            if (load)
                tilesToActivateQueue.Enqueue(tile);
            else
                tilesToDeactivateQueue.Enqueue(tile);
        }

        private void OnEnable()
        {
            OnValidate();
        }

        private void Awake()
        {
            activeObjects = new Dictionary<Coords, GameObject>();
            
            tilesToActivateQueue = new LinkedListQueue<TileInstance>();
            tilesToDeactivateQueue = new LinkedListQueue<TileInstance>();
            
            if (referencePoint == null)
                Debug.LogError("Reference point must be set!");
        }

        /// <summary>
        /// Mainly checks if the child transform object is okay
        /// </summary>
        protected void OnValidate()
        {
            
            if (transform.childCount > 1)
            {
                Debug.LogAssertion("Don't add children to the Tile Map.");
            }
            
            Transform childTransform = transform.Find("TileMap");
            
            if (childTransform == null)
                childTransform = new GameObject("TileMap").transform;
            
            childTransform.SetParent(transform);
            childTransform.localPosition = Vector3.zero;
            childTransform.localRotation = Quaternion.identity;
            childTransform.localScale = Vector3.one;

            tileMapTransform = childTransform;
        }

        /// <summary>
        /// LateUpdate because load order
        /// </summary>
        private void LateUpdate()
        {
            ActivateTilesFromQueue();
            DeactivateTilesFromQueue();
        }
        
        /// <summary>
        /// Calculates the relative (local) tile position in the reference coordinate system
        /// using its coords.
        /// </summary>
        /// <returns>A Vector3 in the form (x, 0, z) </returns>
        protected abstract Vector3 GetPositionOfCoords(Coords coords);

        /// <summary>
        /// Calculates the local tile elevation in the reference coordinate system.
        /// Should not need to override for any use case.
        /// </summary>
        /// <returns>A Vector3 in the form (0, y, 0) </returns>
        protected virtual Vector3 GetElevationOfTile(TileInstance tileInstance)
        {
            return new Vector3(0, tileInstance.elevation * elevationOffsetCoefficient, 0);
        }

        /// <summary>
        /// Local space rotation of newly instantiated tile.
        /// Usually zero, can be other value, for example for triangle tile maps.
        /// </summary>
        protected abstract float GetLocalTileRotation(TileInstance tileInstance);

        /// <summary>
        /// calculate the coordinates of the nearest tile on the grid
        /// must be deterministic. Doesn't need to be exactly precise, +- half a tile is OK.
        /// position - the global position
        /// </summary>
        protected abstract Coords GetNearestTileCoords(Vector3 localPosition);

        /// <summary>
        /// Transforms the vector into local space and calls the GetNearestTileCoords abstract method.
        /// </summary>
        /// <param name="globalPosition">A vector in the global coordinate system</param>
        /// <returns>Coords of the nearest tile</returns>
        public Coords GetNearestTile(Vector3 globalPosition)
        {
            return GetNearestTileCoords(tileMapTransform.InverseTransformPoint(globalPosition));
        }
        
        /// <summary>
        /// Returns nearest coordinates to the Reference point
        /// </summary>
        /// <returns></returns>
        public Coords GetNearestTileToReferencePoint()
        {
            return GetNearestTile(referencePoint.position);
        }

        /// <summary>
        /// Retrieves the local position of a tile, or, if it doesn't exist, the position it would have (minus elevation)
        /// For external use.
        /// </summary>
        public Vector3 GetLocalPositionOfCoords(Coords coords)
        {
            if (activeObjects != null && activeObjects.ContainsKey(coords))
            {
                return activeObjects[coords].transform.localPosition;
            }
            else
            {
                return GetPositionOfCoords(coords);
            }
        }

        /// <summary>
        /// Same as GetLocalPositionOfCoords but in global space
        /// </summary>
        public Vector3 GetGlobalPositionOfCoords(Coords coords)
        {
            return tileMapTransform.TransformPoint(GetLocalPositionOfCoords(coords));
        }

        /// <summary>
        /// Approximate evaluation of tile size. Should consistently underestimate the result.
        /// For external use, e.g. gizmos, etc.
        /// </summary>
        public virtual float GetApproximateTileRadius()
        {
            Coords anyCoords = new Coords(0, 0);
            return GetNeighboringPositions(anyCoords).Select(anyNeighbor =>
                (GetGlobalPositionOfCoords(anyCoords) - GetGlobalPositionOfCoords(anyNeighbor.Value)).magnitude).Min();
        }

        /// <summary>
        /// Returns the gameobject of a tile, or null if it doesn't exist.
        /// Effectively inverse to GetTileInstance().
        /// </summary>
        /// <param name="coords">Coords to get the object for</param>
        /// <returns>The instantiated GameObject of the tile system</returns>
        public GameObject GetGameObject(Coords coords)
        {
            return activeObjects.TryGetValue(coords, out GameObject tileObject) 
                ? tileObject : null;
        }

        /// <summary>
        /// Returns the TileInstance of a tile's gameObject,
        /// or the closest one if the parameter is not one of a tile game object.
        /// Effectively inverse to GetGameObject().
        /// </summary>
        /// <param name="tileGameObject">A Tile's gameobject</param>
        /// <returns>The tile instance.</returns>
        public TileInstance GetTileInstance(GameObject tileGameObject)
        {
            Coords coords = GetNearestTile(tileGameObject.transform.position);
            return tileManager.loadedTiles[coords];
        }

        /// <summary>
        /// Neighbors of a tile position are such tile positions that share an edge (border).
        /// </summary>
        /// <example>
        /// Example for square: GetNeighboringPositions((5, 5)) returns: <br />
        /// 0, (5, 4) <br />
        /// 1, (4, 5) <br />
        /// 2, (5, 6) <br />
        /// 3, (6, 5) </example>
        /// <param name="coords">The coordinates around which to calculate</param>
        /// <returns>A dictionary of direction-coordinates pairs</returns>
        public abstract Dictionary<int, Coords> GetNeighboringPositions(Coords coords);

        /// <summary>
        /// Return the side that is facing the side of the tile's side provided.
        /// </summary>
        /// <param name="side"></param>
        /// <returns></returns>
        public abstract int OppositeSide(int side);

        /// <summary>
        /// Activates a limited amount of tiles from the activate tile queue
        /// </summary>
        protected void ActivateTilesFromQueue()
        {
            int justActivatedTiles = 0;

            while (tilesToActivateQueue.Count > 0 && justActivatedTiles < activateTilesPerFrame)
            {
                TileInstance nextTileToActivate = tilesToActivateQueue.Dequeue();
                
                var newlyInstancedObject = InstantiateTile(nextTileToActivate);

                nextTileToActivate.isActive = true;
                activeObjects.Add(nextTileToActivate.coords, newlyInstancedObject);
                
                tileStats.OnTileActivated();
                justActivatedTiles++;
            }
        }

        /// <summary>
        /// Creates a new GameObject of a tile. 
        /// </summary>
        /// <param name="nextTile">The tile instance to bring to life</param>
        /// <returns>A GameObject ready in the scene.</returns>
        public GameObject InstantiateTile(TileInstance nextTile)
        {
            
            float totalRotation = GetLocalTileRotation(nextTile) + nextTile.mainTileData.currentRotationDegrees;
            
            GameObject newInstance = Instantiate(nextTile.mainTileData.prefab, tileMapTransform);
            Transform newTransform = newInstance.transform;
            
            newTransform.localPosition = GetPositionOfCoords(nextTile.coords) + GetElevationOfTile(nextTile);
            newTransform.localRotation = Quaternion.Euler(0, totalRotation, 0);
            newTransform.localScale = Vector3.one;



            if (nextTile.layerTileData != null)
            {
                foreach (TileData otherTileData in nextTile.layerTileData)
                {
                    if (otherTileData == null)
                        continue;

                    GameObject newLayerInstance = Instantiate(otherTileData.prefab, newTransform);
                    newLayerInstance.transform.localPosition = otherTileData.verticalBorder.offset;
                    newLayerInstance.transform.localRotation = Quaternion.Euler(0, otherTileData.currentRotationDegrees, 0);
                }
            }
            
            return newInstance;
        }

        /// <summary>
        /// Deactivates a limited number of tiles from the queue.
        /// Could also use SetActive(false) instead of Destroy.
        /// </summary>
        protected void DeactivateTilesFromQueue()
        {
            int justDeactivatedTiles = 0;
            
            while (tilesToDeactivateQueue.Count > 0 && justDeactivatedTiles < activateTilesPerFrame)
            {
                TileInstance nextTileToDeactivate = tilesToDeactivateQueue.Dequeue();

                Destroy(activeObjects[nextTileToDeactivate.coords]);

                nextTileToDeactivate.isActive = false;
                activeObjects.Remove(nextTileToDeactivate.coords);

                justDeactivatedTiles++;
            }
            
        }
        
        
    }
}


