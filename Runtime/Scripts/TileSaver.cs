using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;

namespace proj_pcg
{
    /// <summary>
    /// Class keeping track of saved tile state.
    /// Provides an API for loading and saving pooled tiles.
    /// Saves tile positions, types, rotations, elevations and tile layers.
    /// Loaded tiles are "set in stone", unlike InitialState, which is
    /// focused on providing a preferred, or recommended initial state to the system.
    /// </summary>
    public class TileSaver : TileSystemBase
    {
        /// <summary>
        /// If true, saved data will be wiped on start.
        /// </summary>
        [SerializeField] [Tooltip("If true, saved data will be wiped on start.")]
        protected bool resetOnStart;
        
        /// <summary>
        /// Where the tile data should be saved in.
        /// </summary>
        protected string dataFileName => $"{SaveFolder}/tiles.json";
        
        private void Awake()
        {
            // create directory folder if not exists
            Directory.CreateDirectory(SaveFolder);
        }

        /// <summary>
        /// Save all tiles upon end of run time.
        /// </summary>
        private void OnDestroy()
        {
            SaveAllTiles(tileManager.loadedTiles);
        }

        /// <summary>
        /// Override this to change the tile save method
        /// </summary>
        protected virtual void SaveAllTiles(Dictionary<Coords, TileInstance> loadedTiles)
        {
            List<TileInstance> list = loadedTiles.Values.ToList();
            string serialized = JsonConvert.SerializeObject(list);
            File.WriteAllText(dataFileName, serialized);
        }
        
        /// <summary>
        /// Called upon start time to load all tiles.
        /// </summary>
        /// <returns>A dictionary of all tiles loaded from storage</returns>
        public virtual Dictionary<Coords, TileInstance> LoadAllTiles()
        {
            if (!File.Exists(dataFileName) || resetOnStart)
            {
                return new Dictionary<Coords, TileInstance>();
            }
        
            string serialized = File.ReadAllText(dataFileName);

            // only sets position and tileDataId
            var tiles = JsonConvert.DeserializeObject
                <List<TileInstance>>(serialized);

            Dictionary<Coords, TileInstance> result = new();
        
            // set other data
            foreach (TileInstance tile in tiles)
            {
                // DEBUG tile
                if (tile.tileDataId <= -1)
                {
                    continue;
                }

                tile.mainTileData = tileManager.mainLayerTileList[tile.tileDataId];

                tile.layerTileData = new TileData[tile.layerTileIds.Length];

                for (int i = 0; i < tile.layerTileIds.Length; i++)
                {
                    tile.layerTileData[i] = tile.layerTileIds[i] == -1 
                        ? null 
                        : tileManager.additionalLayerList[i][tile.layerTileIds[i]];
                }
                
                result.Add(tile.coords, tile);
            }
            
            return result;
        }
        
        /// <summary>
        /// Use with caution! Removes all current saved DATA from the given generator.
        /// </summary>
        public void RemoveAllSavedData()
        {
            // lazy
            Directory.Delete(SaveFolder, true);
            Directory.CreateDirectory(SaveFolder);
        }
        
    }
}
