using System.IO;
using Newtonsoft.Json;
using UnityEngine;

namespace proj_pcg
{ 
 
    /// <summary>
    /// Serializable class intented solely for keeping track of tile system statistics.
    /// </summary>
    [JsonObject(MemberSerialization.OptOut)]
    public class Stats
    {
        /// <summary>
        /// Copy of the tile manager name
        /// Value added for readibility.
        /// </summary>
        public string tileManagerName;
        
        /// <summary>
        /// How many tile types there are.
        /// </summary>
        public int totalTileTypes;
    
        /// <summary>
        /// How many times was loaded from disk
        /// </summary>
        public int runtimes;
    
        /// <summary>
        /// How many different tile positions were loaded
        /// </summary>
        public int totalTilesGenerated;
    
        [JsonIgnore]
        public int currentTilesLoaded;

        /// <summary>
        /// How many times has any tile been loaded
        /// Can increment multiple times for one position
        /// </summary>
        public int totalTileActivations;

        /// <summary>
        /// Sum of all tile types generated = totalTilesGenerated
        /// </summary>
        public int[] tileTypesGenerated;

        /// <summary>
        /// How many conflicts arised.
        /// </summary>
        public int incompatibleTileCount;

        /// <summary>
        /// Total time elapsed across all run times
        /// </summary>
        public float timeElapsed;
        
        public override string ToString()
        {
            return $"Tile stats for {tileManagerName}:" +
                   $" * loaded {totalTileActivations} total tiles in {runtimes} runs" +
                   $" * across {totalTilesGenerated} different positions" +
                   $" * total time {timeElapsed}" +
                   $" * currently loaded {currentTilesLoaded} tiles";
        }
    }
    
    
    /// <summary>
    /// Class keeping track of generated tiles' statistics.
    /// Provides an API for consistent setting of the stats instance's state.
    /// Loads stats upon startup and saves them upon startdown.
    /// </summary>
    public class TileStats : TileSystemBase
    {
        /// <summary>
        /// If true, saved data will be wiped on start.
        /// </summary>
        [SerializeField] [Tooltip("If true, saved data will be wiped on start.")]
        protected bool resetOnStart;

        /// <summary>
        /// The folder data is to be saved in
        /// </summary>
        protected string statsFileName => $"{SaveFolder}/stats.json";

        
        /// <summary>
        /// A Stats instance for serialization and deserialization
        /// </summary>
        public Stats stats { get; private set; }
        
        private void Awake()
        {
            // create directory folder if not exists
            Directory.CreateDirectory(SaveFolder);
            
            LoadAllStats();
        }

        /// <summary>
        /// Initialize stats
        /// </summary>
        private void Start()
        {
            stats.tileManagerName = tileManager.tileMapName;
            stats.runtimes++;

            int totalTileTypes = tileManager.mainLayerTileList.Count;

            stats.totalTileTypes = totalTileTypes;
            stats.tileTypesGenerated = new int[totalTileTypes];
        }
        
        /// <summary>
        /// Save stats upon end time
        /// </summary>
        private void OnDestroy()
        {
            SaveAllStats(stats);
        }

        private void Update()
        {
            stats.timeElapsed += Time.deltaTime;
        }

        /// <summary>
        /// Deserializes from storage.
        /// Overridable for a different load method.
        /// </summary>
        protected virtual void LoadAllStats()
        {
            if (!File.Exists(statsFileName) || resetOnStart)
            {
                stats = new Stats();
            }
            else
            {
                string serialized = File.ReadAllText(statsFileName);
                stats = JsonConvert.DeserializeObject<Stats>(serialized);
            }
        }

        /// <summary>
        /// Override this to change the stats save method
        /// </summary>
        protected virtual void SaveAllStats(Stats statsToSave)
        {
            string serialized = JsonConvert.SerializeObject(statsToSave);
            File.WriteAllText(statsFileName, serialized);
        }

        /// <summary>
        /// Call each time a tile is generated.
        /// </summary>
        /// <param name="tile">The generated tile instance</param>
        /// <param name="incompatible">true if conflicted state</param>
        public void OnTileGenerated(TileInstance tile, bool incompatible)
        {
            int tileID = tile.tileDataId;
            
            stats.totalTilesGenerated++;

            if (tileID >= 0)
            {
                stats.tileTypesGenerated[tileID]++;
            }

            if (incompatible)
            {
                stats.incompatibleTileCount++;
            }
        }

        /// <summary>
        /// Called each time a tile is activated.
        /// </summary>
        public void OnTileActivated()
        {
            stats.totalTileActivations++;
        }
    }
}
