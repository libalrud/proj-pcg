﻿using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace proj_pcg
{
    /// <summary>
    /// Primitive boilerplate / base code for all components.
    /// Ensures each component can simply communicate with others.
    /// Not the best solution but its ok.
    /// </summary>
    public abstract class TileSystemBase : MonoBehaviour
    {
        /// <summary>
        /// Denotes if all components have been successfully connected.
        /// </summary>
        private bool connected = false;

        private TileManager _tileManager;
        private TileSaver _tileSaver;
        private TileStats _tileStats;
        private Oracle _oracle;
        private Pathfinding _pathfinding;
        private TileMap _tileMap;
        private TerrainMap _terrainMap;
        private InitialState _initialState;
        
        protected TileManager tileManager => LoadComponent(ref _tileManager);

        protected TileSaver tileSaver => LoadComponent(ref _tileSaver);
        protected TileStats tileStats => LoadComponent(ref _tileStats);
        protected Oracle oracle => LoadComponent(ref _oracle);
        protected Pathfinding pathfinding => LoadComponent(ref _pathfinding);
        protected TileMap tileMap => LoadComponent(ref _tileMap);
        protected TerrainMap terrainMap => LoadComponent(ref _terrainMap);
        protected InitialState initialState => LoadComponent(ref _initialState);

        /// <summary>
        /// Loads components if not yet loaded. Returns component.
        /// </summary>
        private T LoadComponent<T>(ref T output) where T: TileSystemBase
        {
            if (!connected || !Application.isPlaying)
            {
                ConnectComponents();
            }
                
            return output;
        }

        /// <summary>
        /// Loads components.
        /// </summary>
        private void ConnectComponents()
        {
            // components are "locked" upon run time
            if (Application.isPlaying) 
                connected = true;

            _tileManager = GetComponent<TileManager>();
            _tileMap = GetComponent<TileMap>();
            _tileSaver = GetComponent<TileSaver>();
            _tileStats = GetComponent<TileStats>();
            _oracle = GetComponent<Oracle>();
            _pathfinding = GetComponent<Pathfinding>();
            _initialState = GetComponent<InitialState>();
            _terrainMap = GetComponent<TerrainMap>();
        
            if (_tileManager == null || _tileMap == null || _tileSaver == null
                || _tileStats == null || _oracle == null || _pathfinding == null 
                || _initialState == null || _terrainMap == null)
            {
                if (Application.isPlaying)
                {
                    connected = false;
                    Debug.LogError("Some components are missing!");
                }
            }
        }

        /// <summary>
        /// Gets string path to save folder using the tile manager name.
        /// </summary>
        protected string SaveFolder
        {
            get
            {
                if (tileManager.tileMapName == "")
                {
                    throw new Exception("Invalid tile loader name!");
                }
        
                return $"{Application.persistentDataPath}/{tileManager.tileMapName}";
            }
        }
    }
}
