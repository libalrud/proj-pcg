using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace proj_pcg
{

    /// <summary>
    /// Wrapper for the Unity Inspector UI to ******' work
    /// </summary>
    [Serializable] 
    public class Layer : IEnumerable<TileData>
    {
        /// <summary>
        /// The basis of a layer: a list of tiles.
        /// </summary>
        [Tooltip("1+ tiles per layer.")]
        public List<TileData> tiles;
        
        public int Count => tiles.Count;
        
        public IEnumerator<TileData> GetEnumerator()
        {
            return tiles.GetEnumerator();
        }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        
        public void Add(TileData newTileData)
        {
            tiles.Add(newTileData);
        }
        
        public void Sort()
        {
            tiles.Sort((a, b) => (int)Mathf.Sign(b.weight-a.weight));
        }
        
        public static Layer operator+(Layer l1, Layer l2)
        {
            Layer l12 = new Layer();

            foreach (TileData tile in l1)
            {
                l12.Add(tile);
            }
            
            foreach (TileData tile in l2)
            {
                l12.Add(tile);
            }

            return l12;
        }

        public TileData this[int i]
        {
            get => tiles[i];
            set => tiles[i] = value as TileData;
        }
        
        public Layer()
        {
            tiles = new List<TileData>();
        }

        public Layer(IEnumerable<TileData> tiles)
        {
            foreach (TileData tile in tiles)
            {
                Add(tile);
            }
        }
    }

    
}