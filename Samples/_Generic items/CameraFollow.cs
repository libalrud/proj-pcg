using System;
using UnityEngine;

namespace Generic_items
{
    public class CameraFollow : MonoBehaviour

    {
        [SerializeField]
        private Transform orientation;
        public float xRot, yRot;

        [SerializeField]
        private float camSens = 0.20f; //How sensitive it with mouse

        private Camera _camera;

        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;

            _camera = GetComponent<Camera>();
        }

        private void FixedUpdate()
        {
            float mouseX = Input.GetAxisRaw("Mouse X") * Time.deltaTime * camSens;
            float mouseY = Input.GetAxisRaw("Mouse Y") * Time.deltaTime * camSens;

            yRot += mouseX;
            xRot -= Mathf.Clamp(mouseY, -90, 90);
            
            transform.rotation = Quaternion.Euler(xRot, yRot, 0);
            orientation.rotation = Quaternion.Euler(0 , yRot, 0);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                _camera.fieldOfView /= 10;
            }
            else if (Input.GetKeyUp(KeyCode.F))
            {
                _camera.fieldOfView *= 10;
            }
        }
    }
}