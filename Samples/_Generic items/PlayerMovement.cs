using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private float speed;
    [SerializeField] private Transform orientation;

    [SerializeField] private bool gravity;

    private float horizontal;
    private float vertical;
    private float updown;

    private float boost;

    private Vector3 moveDirection;

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
    }

    private void FixedUpdate()
    {
        MovePlayer();
    }

    // Update is called once per frame
    private void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxis("Vertical");

        boost = Input.GetKey(KeyCode.E) ? 10 : 1;

        updown = Input.GetKey(KeyCode.Space) ? 1 : 0;
        updown += Input.GetKey(KeyCode.LeftShift) ? -1 : 0;

        if (Input.GetKeyDown(KeyCode.G))
        {
            gravity =! gravity;
        }
    }

    private void MovePlayer()
    {
        moveDirection = orientation.forward * vertical + orientation.right * horizontal + orientation.up * updown;

        Vector3 movementForce = moveDirection.normalized * (boost * speed);

        if (gravity)
        {
            movementForce += 10 * Vector3.down;
        }

        rb.AddForce(movementForce, ForceMode.Force);
    }
}
